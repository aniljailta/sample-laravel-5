<?php

	function canView($elementName)
	{
		if(\Auth::user()->permission_code == 2)
			return true;

		if(is_array( \Session::get('user.elements'))){
			if(in_array($elementName, \Session::get('user.elements')))
			return true;
		else
			return false;
		}
		return false;
	}

?>
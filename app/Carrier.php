<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carrier extends Model {

	use SoftDeletes;

	protected $table = 'carrier';

	protected $primaryKey = 'carrier_id';

	protected $dates = ['deleted_at'];

	protected $guarded = ['carrier_id'];


	/**
	 * [search by mover Id]
	 * @param  [object] $query [description]
	 * @return [type]        [description]
	 */
	public function scopeSearchById($query,$id)
	{
		return $query->where('carrier_id',$id);
	}

	public function defaultdata(){

     return $this->HasMany('App\DefaultData','carrier_id','carrier_id');

    }

}

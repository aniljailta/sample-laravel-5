<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResidenceArea extends Model {

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'location_area';
	
    protected $primaryKey = 'location_area_id';

	protected $fillable = ['location_id','area_id','carrier_id','last_edited_by'];

	//public $timestamps = false;

	/**
	 * [search by mover Id]
	 * @param  [object] $query [description]
	 * @return [integer]        [residence id]
	 */
	public function scopeSearchByResidenceId($query,$id,$carrier_id)
	{
		return $query->where('location_id',$id)->where('carrier_id',$carrier_id);
	}

	/**
	 * [search by mover Id]
	 * @param  [object] $query [description]
	 * @return [integer]        [residence id]
	 * @return [integer]        [room id]
	 */
	public function scopeSearchByResidenceRoom($query,$roomid,$residenceid,$carrier_id)
	{
		return $query->where('area_id',$roomid)->where('location_id',$residenceid)->where('carrier_id',$carrier_id);
	}

}

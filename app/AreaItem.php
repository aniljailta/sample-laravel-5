<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaItem extends Model {

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'area_item';

	protected $primaryKey = 'area_item_id';

	protected $fillable = ['item_count', 'item_id', 'carrier_id','area_id','last_edited_by'];

	
	/**
	 * [join area_item table with item table]
	 * @param  [object] $query
	 * @param  [integer] $residenceId [residenceId] [carrier_id]
	 */
	public function scopeItems($query, $residenceId,$carrier_id)
	{
		return $query->join('item', 'item.item_id', '=', 'area_item.item_id')->where('area_id',$residenceId);
	}

	/**
	 * [search by areaId and itemId carrier_id]
	 * @param  [object] $query  [description]
	 * @param  [integer] $roomId [description]
	 * @param  [integer] $itemId [description]
	 * @return [type]         [description]
	 */
	public function scopeSearchByAreaItem($query, $roomId, $itemId,$carrier_id)
	{
		return $query->where('area_id',$roomId)->where('item_id',$itemId)->where('carrier_id',$carrier_id);
	}

}

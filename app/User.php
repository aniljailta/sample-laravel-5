<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

	use Authenticatable, CanResetPassword;

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'backend-user';


    protected $primaryKey = 'backend-user_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = ['first_name', 'last_name', 'password', 'email', 'phone1', 'is_active','permission-level_id', 'carrier_id','permission_code'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * [ user belongs to a PermisionLevel ]
	 * @return [object] [description]
	 */
	public function permission()
	{
		return $this->belongsTo('\App\PermissionLevel','permission_code','permission_code');
	}

	/**
	 * [ user attached to a carrier ]
	 * @return [object] [description]
	 */
	public function carrier()
	{
		return $this->belongsTo('\App\Carrier','carrier_id','carrier_id');
	}

	/**
	 * left join with permission-level table
	 * @param  [object] $query [description]
	 * @return [object]        [description]
	 */
	public function scopeJoinPermissionLevel($query)
	{
		return $query->leftJoin('permission-level','permission-level.permission_code','=','backend-user.permission_code');
	}

	/**
	 * left join with carrier table
	 * @param  [object] $query [description]
	 * @return [object]        [description]
	 */
	public function scopeJoinCarrier($query)
	{
		return $query->leftJoin('carrier','carrier.carrier_id','=','backend-user.carrier_id');
	}

	 public function getBackendUserIdAttribute($value)
    {
        return $value;
    }

}

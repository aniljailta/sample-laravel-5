<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model {

	use SoftDeletes;

	protected $table = 'area';

	protected $guarded = ['area_id'];

	protected $primaryKey = 'area_id';

	protected $dates = ['deleted_at'];

	/**
	 * [search by mover Id]
	 * @param  [object] $query [description]
	 * @return [type]        [area id]
	 */
	public function scopeSearchById($query,$id)
	{
		return $query->where('area_id',$id);
	}

	/**
	 * [find items in roomids]
	 * @param  [object] $query    [description]
	 * @param  [array] $roomsIds [room ids]
	 * @return [type]           [description]
	 */
	public function scopeItems($query,$roomsIds)
	{
		return $query->whereIn('area_id',$roomsIds);
	}

}

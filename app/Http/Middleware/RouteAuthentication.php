<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class RouteAuthentication {

	/**
	 * Handle an incoming request and filter for authorization
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (Auth::guest())
		{
				return redirect('un-authorized');
		}

		if(Auth::user()->permission_code != 2){

			$routePermissions = \App\PermissionResource::resources(Auth::user()->permission_code)->lists('method_uri');
			$routeUri = \Route::getCurrentRoute()->getUri();
			$method = \Route::getCurrentRoute()->getMethods()[0];

			if(!in_array($method."->".$routeUri, $routePermissions))
			{
				return redirect('un-authorized');
			}
		}

		return $next($request);
	}

}

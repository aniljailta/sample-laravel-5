<?php namespace App\Http\Middleware;

use Closure;

class ComponentMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(\Auth::user()->permission->elements->count() > 0){
				\Session::forget('user.elements');
				foreach (\Auth::user()->permission->elements as $element) {
					if(isset($element->element->element_name))
					\Session::push('user.elements', $element->element->element_name);
				//dd(\Session::get('user.elements'));
				}
		}
		else{
				\Session::forget('user.elements');
		}
		return $next($request);
	}

}

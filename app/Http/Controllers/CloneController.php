<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CloneController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function cloneData(Request $req)
	{
	    $carrier_id = $req->input('carrier_id');
		$check = \App\DefaultData::where('carrier_id',$carrier_id)->where('flag','1')->count();
		if($check){ echo json_encode(array('success'=>'201')); return; } else 
		if(ctype_digit($carrier_id) && ($carrier_id > 0)){
		   //area item clone with logging id to temp table
		   $count_area = \App\Area::where('carrier_id',$carrier_id)->count(); 
		   if(!$count_area){		   
			   $areas = \App\Area::where('carrier_id','0')->get();		   
			   foreach ($areas as $areadefault){
			   
				  $area = new \App\Area();
				  $area->carrier_id = 	$carrier_id;
				  $area->area_type = 	$areadefault->area_type;
				  $area->save();
				  
				  $areaTemp  =  new \App\AreaTemp();
				  $areaTemp->default_area_id = $areadefault->area_id;
				  $areaTemp->new_area_id = $area->area_id; 			  
				  $areaTemp->carrier_id = $carrier_id;
				  $areaTemp->save();
				  
				}
            } 
             
			 //item table replication and and make a temp id log
			    
			 $count_area = \App\Item::where('carrier_id',$carrier_id)->count(); 
		     if(!$count_area){
              $items  = \App\Item::where('carrier_id','0')->get();
			  foreach($items as $item)
			  {			 
                //item table replication   
			    $item = \App\Item::findOrFail($item->item_id);

				// create new object
				$newItem = $item->replicate();
                $newItem->carrier_id = $carrier_id; 
				// make any required changes to object
				$newItem->save(); 
				 
				$itemAddedCharge = \App\ItemAddedCharge::where('item_id',$item->item_id)->first();
				if(isset($itemAddedCharge->{'item-added-charges_id'})){
				  //item added charge table replication   
					$itemAddedCharge = \App\ItemAddedCharge::findOrFail($itemAddedCharge->{'item-added-charges_id'});
					$itemAddedCharge = $itemAddedCharge->replicate();
					$itemAddedCharge->item_id = $newItem->item_id;; 
					$itemAddedCharge->save();   
				} 	
				
				$itemTemp  =  new \App\ItemTemp();
				$itemTemp->default_item_id = $item->item_id;
				$itemTemp->new_item_id = $newItem->item_id; 			  
				$itemTemp->carrier_id = $carrier_id;
				$itemTemp->save();
		    }		
           }

          //make area item association table ; 
		       
		       $count  = \App\AreaItem::where('carrier_id',$carrier_id)->count();
               if(!$count){
			        
					$areaitem  = \App\AreaItem::where('carrier_id','0')->get();
                    foreach($areaitem as $area){
					
					    $item = \App\AreaItem::findOrFail($area->area_item_id);
						$areaItem = $item->replicate();
						
						$areaItem->carrier_id = $carrier_id;
						
                        $oldItemID = \App\ItemTemp::where('default_item_id',$item->item_id)->first();
						$areaItem->item_id = $oldItemID->new_item_id;
						
						$oldAreaID = \App\AreaTemp::where('default_area_id',$item->area_id)->first();
						$areaItem->area_id = $oldAreaID->new_area_id;
						
						$areaItem->save();        
                  
					}
					
               }

             //update location cloned data
             $locations  = \App\Residence::where('carrier_id','0')->get();
			 foreach($locations as $loc)
			  {			 
                $location = \App\Residence::findOrFail($loc->location_id);

				$newLoc = $location->replicate();
                $newLoc->carrier_id = $carrier_id; 
				$newLoc->save(); 
				
				//temp table to save original id relation
            	
                $itemTemp  =  new \App\ResidenceTemp();
				$itemTemp->default_location_id = $loc->location_id;
				$itemTemp->new_location_id = $newLoc->location_id; 			  
				$itemTemp->carrier_id = $carrier_id;
				$itemTemp->save();
		     
			 }
			 
	       //location area data   		 
	        
            $location_areas =  \App\ResidenceArea::where('carrier_id','0')->get();
			foreach($location_areas as $locArea){             
			 $location = \App\ResidenceArea::findOrFail($locArea->location_area_id);

				$newLocArea = $location->replicate();
                $newLocArea->carrier_id    = $carrier_id;
				
                $oldAreaID = \App\AreaTemp::where('default_area_id',$location->area_id)->where('carrier_id',$carrier_id)->first();
				$newLocArea->area_id       = $oldAreaID->new_area_id;
				
				$locDeaf   = \App\ResidenceTemp::where('default_location_id',$location->location_id)->first(); 
                $newLocArea->location_id   = $locDeaf->new_location_id;				
				$newLocArea->save();  

            }	

          //moving rate clone 
          $moving_rates  =  \App\MovingRate::where('carrier_id','0')->get();
		    foreach($moving_rates  as $rates ){
               $rate  =  \App\MovingRate::findOrFail($rates->{'moving-rate_id'});
               $newRate = $rate->replicate();
               $newRate->carrier_id = $carrier_id; 
			   $newRate->save();
             }			
         
		 //now add flag to default log table

 		 $DefaultData =  new \App\DefaultData();
		 $DefaultData->carrier_id  = $carrier_id;
		 $DefaultData->last_edited_by = \Auth::user()->getAttribute('backend-user_id');
         $DefaultData->flag  = '1';
         if($DefaultData->save()){
            echo json_encode(array('success'=>'200'));	
		 }		 
 		 	
	   } 
	}


}

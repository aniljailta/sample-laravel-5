<?php namespace App\Http\Controllers;
use DB;
use App\Services\QuoteService;
use Carbon\Carbon;
use App\User;
use App\Quote;
class HomeController extends LoggedController {

	/**
	 * Create a new controller instance.
	 *
	 */
	protected $quoteService;

	public function __construct(QuoteService $quoteService)
	{
		parent::__construct();

		$this->quoteService = $quoteService;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$data['page_title'] = 'Home';
		$data['user_profile'] = $this->user;

		//count quote due in next 10 days;
		$today = date('Y-m-d');
		$date=strtotime($today);
		$newDate = date('Y-m-d',strtotime('+10 days',$date));
		$yesterday = date('Y-m-d',strtotime('-1 days',$date));

		$count_quote = Quote::CountQuotes()->get();
		$quotes_count = [];
		foreach($count_quote as $quote){
		  if(empty($quote->agreement_signature_date)){
		     $quotes_count[] = $quote->quote_id;
		  }
		}
		$count_quote = count($quotes_count);

		$open_quote_count = Quote::OpenQuoteCount($today)->count();

		$data['quote_count'] = $count_quote;

		$quote_detail = $this->quoteService->getBookedMove();
		$quote_alert =  $this->quoteService->getBookedMoveTodayNoti();

		$data['quote_detail'] = $quote_detail;
		$data['alerts'] = $quote_alert;
		$data['open_quote_count'] = $open_quote_count;

		return view('home',$data);
	}


	public function getQuotesForCal(){
	   $quote_detail = $this->quoteService->getBookedMove();
	   echo json_encode(array('success'=>$quote_detail));

	}

	//shift current login as last login
	public function logined(){

      $time = Carbon::now();
	  $user = User::find($this->user->getAttribute('backend-user_id'));
      $user->last_login_date = $user->current_login_date;
      $user->current_login_date = $time;
      $user->save();

	  return redirect('home');

	}
	//if not active user
	public function notActive(){

	  return view('notactive');

	}

	/**
	 * [un-authorized user]
	 * @return [view] [show un-authorization messafe to user]
	 */
	public function unAuthorized(){

	 $page_title='Notice Page';

	 return view('un-authorized', compact('page_title'));

	}
}

<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 15-2-10
 * Time: 4:48 PM
 */

use App\Services\QuoteService;

class ScheduleController extends LoggedController {

	/**
	 * Create a new controller instance.
	 *
	 */
	public function __construct(QuoteService $quoteService)
	{
		parent::__construct();

		$this->quoteService = $quoteService;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->data['page_title'] = 'Schedule';

		$quote_detail = $this->quoteService->getBookedMove();

		$this->data['quote_detail'] = $quote_detail;
        //dump($quote_detail);exit();
		return view('schedule',$this->data);
	}

}
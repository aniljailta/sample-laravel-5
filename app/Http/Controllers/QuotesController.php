<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 15-2-10
 * Time: 4:48 PM
 */
use Illuminate\Http\Request;
use DB;
use App\Services\QuoteService;
use App\Offer;

class QuotesController extends LoggedController
{
	protected $quoteService;

	/**
	 * Create a new controller instance.
	 *
	 */
	public function __construct(QuoteService $quoteService)
	{
		parent::__construct();

		$this->quoteService = $quoteService;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->data['page_title'] = 'Quotes and Booked Moves';

		$this->data['quotes'] = $this->quoteService->getBookedMoveToday('current');

		return view('quotes', $this->data);
	}

	public function getQuote($id){
		$result['quote_detail'] = $this->quoteService->getQuote($id);
		//$result['mover_detail'] = $this->quoteService->getMover($result['quote_detail']->MoverId);
		$result['shipper_detail'] = $this->quoteService->getOwner($result['quote_detail']->owner_id);

		echo json_encode($result);
	}

	public function quoteDetail($id){

	$quotes = $this->quoteService->getQuoteList();

	 foreach($quotes as $key=>$quote){
	  if($quote->QuoteId  == $id){
         $selcted_key =  $key;
         $new_value = $quotes[$selcted_key];
		 unset($quotes[$selcted_key]);
		 array_unshift($quotes, $new_value);
       }
	 }

	 //print_r($quotes);
	 $this->data['page_title'] = 'Quotes and Booked Moves';
	 $this->data['quotes'] = $quotes;
	 $this->data['new'] = 'yes';
     //print_r($quotes);
	 return view('quotes', $this->data);

	}

	//sort quote
	public function sortQuote(Request $q){

	  $type =  $q->input('type');

	  $quotes = $this->quoteService->getBookedMoveToday($type);
	  //echo "<pre>";
	  echo json_encode(array('success'=>$quotes));
	}
	//currentoffer

	public function currentOffer(Request $q){
	   $id  =  $q->input('id');
	   $data = Offer::SearchByQuoteId($id)->get();
	   echo json_encode(array('success'=>$data));
	}
}
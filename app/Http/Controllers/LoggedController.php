<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 15-2-12
 * Time: 2:30 PM
 */

use Auth;

class LoggedController extends Controller
{

	public $user;
	public $data;

	/**
	 * Create a new controller instance.
	 *
	 */
	public function __construct()
	{
		$this->middleware('auth');
        //$this->middleware('Isactive');
		$this->user = Auth::user();
		$this->data['user_profile'] = Auth::user();
	}
}

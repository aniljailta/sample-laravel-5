<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class RetriveController extends Controller {

	/**
	 * Display list of deleted view elements
	 * @return response
	 */
	public function trashedViewElements()
	{
		$viewElements = \App\ViewElement::onlyTrashed()->get();
		$page_title = 'Deleted View Elements';

		return view('trashed.view-elements', compact('viewElements', 'page_title'));
	}

	/**
	 * restore a record from ViewElement by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreViewElementsById(Request $request)
	{
		$id = $request->input('id');
		\App\ViewElement::withTrashed()->whereId($id)->restore();

		echo json_encode(array('response'=>'success'));
	}


	/**
	 * Display list of deleted items
	 * @return response
	 */
	public function trashedItems()
	{
		$items = \App\Item::onlyTrashed()->get();
		$page_title = 'Deleted Items';

		return view('trashed.items', compact('items', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreItemsById(Request $request)
	{
		$id = $request->input('id');
		\App\Item::withTrashed()->where('item_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted additional req
	 * @return response
	 */
	public function trashedAdditionalReq()
	{
		$items = \App\AdditionalReq::onlyTrashed()->get();
		$page_title = 'Deleted Additional Requests';

		return view('trashed.additional-req', compact('items', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreAdditionalReqById(Request $request)
	{
		$id = $request->input('id');
		\App\AdditionalReq::withTrashed()->where('id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted area
	 * @return response
	 */
	public function trashedArea()
	{
		$items = \App\Area::onlyTrashed()->get();
		$page_title = 'Deleted Areas';

		return view('trashed.area', compact('items', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreAreaById(Request $request)
	{
		$id = $request->input('id');
		\App\Area::withTrashed()->where('area_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted backend user
	 * @return response
	 */
	public function trashedBackendUser()
	{
		$users = \App\User::onlyTrashed()->get();
		$page_title = 'Deleted Backend Users';

		return view('trashed.backend-user', compact('users', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreBackendUserById(Request $request)
	{
		$id = $request->input('id');
		\App\User::withTrashed()->where('backend-user_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}


	/**
	 * Display list of deleted category name
	 * @return response
	 */
	public function trashedCategoryName()
	{
		$names = \App\CategoryName::onlyTrashed()->get();
		$page_title = 'Deleted Category Name';

		return view('trashed.category-name', compact('names', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreCategoryNameById(Request $request)
	{
		$id = $request->input('id');
		\App\CategoryName::withTrashed()->where('CategoryNameID',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted category type
	 * @return response
	 */
	public function trashedCategoryType()
	{
		$types = \App\CategoryType::onlyTrashed()->get();
		$page_title = 'Deleted Category Type';

		return view('trashed.category-type', compact('types', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreCategoryTypeById(Request $request)
	{
		$id = $request->input('id');
		\App\CategoryType::withTrashed()->where('CategoryTypeID',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted item group
	 * @return response
	 */
	public function trashedItemGroup()
	{
		$items = \App\ItemGroup::onlyTrashed()->get();
		$page_title = 'Deleted Item Group';

		return view('trashed.item-group', compact('items', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreItemGroupById(Request $request)
	{
		$id = $request->input('id');
		\App\ItemGroup::withTrashed()->where('ItemGroupId',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted mover
	 * @return response
	 */
	public function trashedCarrier()
	{
		$carriers = \App\Carrier::onlyTrashed()->get();
		$page_title = 'Deleted Carrier';

		return view('trashed.mover', compact('carriers', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreCarrierById(Request $request)
	{
		$id = $request->input('id');
		\App\Carrier::withTrashed()->where('carrier_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted offer
	 * @return response
	 */
	public function trashedOffer()
	{
		$offers = \App\Offer::onlyTrashed()->get();
		$page_title = 'Deleted Offers';

		return view('trashed.open-route', compact('offers', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreOfferById(Request $request)
	{
		$id = $request->input('id');
		\App\Offer::withTrashed()->where('OfferId',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted open routes
	 * @return response
	 */
	public function trashedOpenRoute()
	{
		$routes = \App\OpenRoute::onlyTrashed()->get();
		$page_title = 'Deleted Open Routes';

		return view('trashed.open-route', compact('routes', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreOpenRouteById(Request $request)
	{
		$id = $request->input('id');
		\App\OpenRoute::withTrashed()->where('open-route_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted packing information
	 * @return response
	 */
	public function trashedPackingInformation()
	{
		$items = \App\PackingInformation::onlyTrashed()->get();
		$page_title = 'Deleted Packing Information';

		return view('trashed.packing-information', compact('items', 'page_title'));
	}

	/**
	 * restore a record from Item by id
	 * @param  integer $id
	 * @return response
	 */
	public function restorePackingInformationById(Request $request)
	{
		$id = $request->input('id');
		\App\PackingInformation::withTrashed()->where('TableOutputSequence',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted permission level
	 * @return response
	 */
	public function trashedPermissionLevel()
	{
		$levels = \App\PermissionLevel::onlyTrashed()->get();
		$page_title = 'Deleted Permission Level';

		return view('trashed.permission-level', compact('levels', 'page_title'));
	}

	/**
	 * restore a record from permission level by id
	 * @param  integer $id
	 * @return response
	 */
	public function restorePermissionLevelById(Request $request)
	{
		$id = $request->input('id');
		\App\PermissionLevel::withTrashed()->where('permission-level_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted residence
	 * @return response
	 */
	public function trashedResidence()
	{
		$residences = \App\Residence::onlyTrashed()->get();
		$page_title = 'Deleted Residence';

		return view('trashed.residence', compact('residences', 'page_title'));
	}

	/**
	 * restore a record from residence level by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreResidenceById(Request $request)
	{
		$id = $request->input('id');
		\App\Residence::withTrashed()->where('ResidenceId',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted residence
	 * @return response
	 */
	public function trashedOwner()
	{
		$owners = \App\Owner::onlyTrashed()->get();
		$page_title = 'Deleted Owner';

		return view('trashed.owner', compact('owners', 'page_title'));
	}

	/**
	 * restore a record from Shipper level by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreOwnerById(Request $request)
	{
		$id = $request->input('id');
		\App\Owner::withTrashed()->where('owner_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted residence
	 * @return response
	 */
	public function trashedState()
	{
		$states = \App\State::onlyTrashed()->get();
		$page_title = 'Deleted State';

		return view('trashed.state', compact('states', 'page_title'));
	}

	/**
	 * restore a record from State level by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreStateById(Request $request)
	{
		$id = $request->input('id');
		\App\State::withTrashed()->where('state_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}


	/**
	 * Display list of deleted quotes
	 * @return response
	 */
	public function trashedQuote()
	{
		$quotes = \App\Quote::onlyTrashed()->get();
		$page_title = 'Deleted Quote';

		return view('trashed.quote', compact('quotes', 'page_title'));
	}

	/**
	 * restore a record from quote level by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreQuoteById(Request $request)
	{
		$id = $request->input('id');
		\App\Quote::withTrashed()->where('quote_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted rates
	 * @return response
	 */
	public function trashedMovingRate()
	{
		$rates = \App\MovingRate::onlyTrashed()->get();
		$page_title = 'Deleted Moving Rate';

		return view('trashed.moving-rate', compact('rates', 'page_title'));
	}

	/**
	 * restore a record from quote level by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreMovingRateById(Request $request)
	{
		$id = $request->input('id');
		\App\MovingRate::withTrashed()->where('moving-rate_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted rates
	 * @return response
	 */
	public function trashedCountyZipCode()
	{
		$codes = \App\CountyZipCode::onlyTrashed()->get();
		$page_title = 'Deleted County Zip Code';

		return view('trashed.county-zip-code', compact('codes', 'page_title'));
	}

	/**
	 * restore a record from quote level by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreCountyZipCodeById(Request $request)
	{
		$id = $request->input('id');
		\App\CountyZipCode::withTrashed()->where('county-zip-code_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

	/**
	 * Display list of deleted item added charges
	 * @return response
	 */
	public function trashedItemAddedCharge()
	{
		$items = \App\ItemAddedCharge::onlyTrashed()->get();
		$page_title = 'Deleted Item Added Charges';

		return view('trashed.item-added-charges', compact('items', 'page_title'));
	}

	/**
	 * restore a record from item added charges level by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreItemAddedChargeById(Request $request)
	{
		$id = $request->input('id');
		\App\ItemAddedCharge::withTrashed()->where('item-added-charges_id',$id)->restore();

		echo json_encode(array('response'=>'success'));
	}

}

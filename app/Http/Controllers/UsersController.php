<?php namespace App\Http\Controllers;

/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 15-2-10
 * Time: 4:48 PM
 */

use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use DB;
use Carbon\Carbon;

class UsersController extends LoggedController
{
	protected $registrar;

	protected $auth;

	/**
	 * Create a new controller instance.
	 *
	 */
	public function __construct(Guard $auth)
	{
		parent::__construct();

		$this->auth = $auth;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$permission =  \App\PermissionLevel::all();
		$companies  =  \App\Carrier::select('carrier_id','company_web_name')->get();
		$page_title = 'Users';
		$users =  \App\User::JoinPermissionLevel()->get();
		return view('users', compact('permission','companies','page_title','users'));
	}

	

	/**
	 * [save changed password]
	 * @param  Request $request [Request object]
	 * @return [message]        [ status message]
	 */
	public function postChangePassword(Request $request)
	{
		$this->data['page_title'] = 'Change password';

		$new_user = $request->all();
       if(!empty($new_user['old_password'])){

		if ( ! $this->auth->validate(['email' => $this->user->email, 'password' => $new_user['old_password']]) ) {
			$this->data['old_pwd_errors'] = 'Old password is not correct';
            echo json_encode(array('error'=>'Old password is not correct'));
			//return view("change_password", $this->data);
		 }
		}
		else {
			$validator = Validator::make($new_user, [
				//'old_password' => 'required',
				'new_password' => 'required|Confirmed|min:8',
				'new_password_confirmation' => 'required',
			]);

			if ( $validator->fails() ) {
				$this->throwValidationException(
					$request, $validator
				);
			}

			$user = User::find($new_user['backendUserId']);

			$user->password = bcrypt($new_user['new_password']);
			$user->last_edited_by =  $this->user->id;
            $user->updated_at = Carbon::now();
            $user->save();
            $this->data['success'] = 'Updated password successfully!';
            echo json_encode(array('success'=>200));
			//return view("change_password", $this->data);
		}
	}


	/**
	 * [save posted data to add a new user]
	 * @param  Request $request [Request obkect]
	 * @return [response]
	 */
	public function postAddUser(Request $request)
	{
		$this->data['page_title'] = 'Add User';

		$new_user = $request->all();

		$validator = Validator::make($new_user, [
			'first-name' => 'required|max:255',
			'last-name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:backend-user',
			'password' => 'required|confirmed|min:6',
			'phone' => 'min:4|max:20',
		]);

		if ( $validator->fails() ) {
			$this->throwValidationException(
				$request, $validator
			);
		}
		$result = User::create([
			'first_name' => $new_user['first-name'],
			'last_name' => $new_user['last-name'],
			'email' => $new_user['email'],
			'password' => bcrypt($new_user['password']),
			'phone1' => $new_user['phone'],
			'is_active' => $new_user['is_active'],
			'permission_code' => $new_user['PermissionId'],
			'carrier_id' => $new_user['CarrierId'],
			'last_edited_by' =>  $this->user->id,
			'updated_at'=> Carbon::now()

		]);

		if ( $result->id > 0 ) {
			$this->data['success'] = 'Added user successfully!';
		}
		else {
			$this->data['custom_errors'] = 'Failed to add new user!';
		}

        $users =  \App\User::JoinPermissionLevel()->get()->toArray();

		 foreach($users as $key=>$quote){
			  if($quote['backend-user_id']   == $result->getAttribute('backend-user_id')){
				 $selcted_key =  $key;
			   }
			 }
			 $new_value = $users[$selcted_key];
			 unset($users[$selcted_key]);
			 array_unshift($users, $new_value);

		     echo json_encode(array('success'=>$users));
		//return view("add_user", $this->data);
	}

	//get one user
	public function postGetOne(Request $req){
	 $id =  $req->input('id');

	 $user = User::find($id);
	 echo json_encode(array('success'=>$user));

	}

	public function deleteUser()
	{
		if ( isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) {
			//ajax request
			$user_ids = explode(',', $_POST['user_ids']);

			$response['error'] = true;
			if ( User::destroy($user_ids) > 0 ) {
				$response['error'] = false;
			}
            $users =  User::JoinPermissionLevel()->get();
			echo json_encode(array('success'=>$users));
		}
		else {
			abort(404);
		}
	}


	/**
	 * [save edited user data]
	 * @param  Request $request [Request object]
	 * @return [response]           [send response to ajax call]
	 */
	public function postEditUser(Request $request)
	{
		$new_user = $request->all();

		$validator = Validator::make($new_user, [
			'first_name' => 'required|max:255',
			'last_name' => 'required|max:255',
			'phone1' => 'min:4|max:20',
			'emailread' => 'unique:backend-user,email,'.$new_user['backendUserId'].',backend-user_id',
		]);

		if ( $validator->fails() ) {
			$this->throwValidationException(
				$request, $validator
			);
		}

		$user = User::find($new_user['backendUserId']);
		$user->email = $new_user['emailread'];
		$user->first_name = $new_user['first_name'];
		$user->last_name = $new_user['last_name'];
		$user->phone1 = $new_user['phone1'];
		$user->carrier_id    =  $new_user['CarrierIdEdit'];
		$user->permission_code =  $new_user['PermissionId'];
		$user->is_active = $new_user['is_active'];
		$user->last_edited_by = $this->user->getAttribute('backend-user_id');
        $user->updated_at = Carbon::now();


		$user->save();
        //$this->lastupdate();

		$users =  User::JoinPermissionLevel()->get()->toArray();

		foreach($users as $key=>$quote){
			  if($quote['backend-user_id']   == $user->getAttribute('backend-user_id')){
				 $selcted_key =  $key;
			   }
	    }
	    $new_value = $users[$selcted_key];
		unset($users[$selcted_key]);
		array_unshift($users, $new_value);

		echo json_encode(array('success'=>$users));
		//return redirect('/users');
	}


	/**
	 * Display list of deleted resources
	 * @return response
	 */
	public function trashed()
	{
		$viewElements = \App\User::onlyTrashed()->get();
		$page_title = 'Deleted View Elements';

		return view('view-elements.trashed', compact('viewElements', 'page_title'));
	}

	/**
	 * resotre deleted rows from view element
	 * @return response
	 */
	public function restoreAll()
	{
		$viewElements = \App\User::onlyTrashed()->get();
		foreach ($viewElements as $element) {
			$element->restore();
		}
		\Flash::info('Data restored from view elements');

		return redirect('view-elements');
	}

	/**
	 * restore a row by id
	 * @param  integer $id
	 * @return response
	 */
	public function restoreById($id)
	{
		\App\User::withTrashed()->whereId($id)->restore();
		\Flash::info('Data restored');

		return redirect('view-elements');
	}

}
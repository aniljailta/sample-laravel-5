<?php namespace App\Http\Controllers;
/**
 * Created by PhpStorm.
 * User: Bowen
 * Date: 15-2-10
 * Time: 4:48 PM
 */

class StatisticsController extends LoggedController {

	/**
	 * Create a new controller instance.
	 *
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['page_title'] = 'Statistics';
		$data['user_profile'] = $this->user;
		return view('statistics',$data);
	}

}
<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\PermissionLevel;

class PermissionLevelController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$permissionLevels = \App\PermissionLevel::where('permission_code','!=',0)->get();
		$routes = \Route::getRoutes();
		$openRoutes = \App\OpenRoute::lists('route');
		$openRoutesAll = \App\OpenRoute::all();

        $viewElements = \App\ViewElement::orderBy('element_group')->get();
		$elementGroup = null;

		$viewElementsAll = \App\ViewElement::all();
		$groups = \App\ViewElement::distinct('element_group')->lists('element_group');

		$carriers  = \App\Carrier::with('defaultdata')->get();

		$page_title = 'Administrative Settings';
		return view('admin', compact('permissionLevels','page_title','routes','openRoutes','openRoutesAll','elementGroup','viewElements','viewElementsAll','groups','carriers'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$page_title ='Create new permission level';

		return view('permissions.create', compact('page_title'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make(['permission_label' => $request->input('PermissionName')], [
				'permission_label' => 'required|max:255'
			]);

		if ( $validator->fails() ) {
			$this->throwValidationException(
				$request, $validator
			);
		}

		if(ctype_digit($request->input('PermissionId'))){

			  //update field
			   $permissionLevelCurrent = \App\PermissionLevel::where('permission-level_id',$request->input('PermissionId'))->first();

				   if($request->input('PermissionType') != $permissionLevelCurrent->PermissionType){
						 //get max value for selected type
						 $PermissionId = \App\PermissionLevel::where('permission_type',$request->input('PermissionType'))->max('PermissionId');
						 $permissionLevel['PermissionId']   = $PermissionId;
						 $permissionLevel['PermissionType'] = $request->input('PermissionType');
				   }
			   $permissionLevel['PermissionName'] = $request->input('PermissionName');

			   \App\PermissionLevel::where('PermissionLevelId',$permissionLevelCurrent->PermissionLevelId)->update($permissionLevel);
		}
		else
		{

		        $id = \App\PermissionLevel::where('permission_type',$request->input('PermissionType'))->max('permission-level_id');
			    $PermissionId  = $id + 1;

				$permissionLevel= new \App\PermissionLevel();
				$permissionLevel->permission_label = $request->input('PermissionName');
				//$permissionLevel->permission-level_id   = $PermissionId;
				$permissionLevel->permission_type = $request->input('PermissionType');
				$permissionLevel->save();
		}

		$permissionLevels = \App\PermissionLevel::where('permission-level_id','!=',0)->get();

		echo json_encode(array('success'=>$permissionLevels));

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		$id =  $request->input('id');
		$count = \App\User::where('permission-level_id',$id)->count();
		If(!$count){
		   $id = \App\PermissionLevel::where('permission-level_id',$id)->delete();
		   $permissionLevels = \App\PermissionLevel::where('permission-level_id','!=',0)->get();
		}
		else
		{
		  $permissionLevels = 300;
		}

		echo json_encode(array('success'=>$permissionLevels));

	}

	/**
	 * [show resource list for permission level]
	 * @param  [integer] $id [permission level id]
	 * @return [view]     [show list of resources for permission level]
	 */
	public function getPermissionResource(Request $request)
	{
		$id =  $request->input('id');
		$permissionLevelId = $id;
		$routePermissions = \App\PermissionResource::resources($permissionLevelId)->get();

		echo json_encode(array('success'=>$routePermissions));
	}

	/**
	 * [save resource list for a permission level]
	 * @param  [integer]  $id      [permission level id]
	 * @param  Request $request [Request object]
	 * @return [redirection]           [redirect to permission levels list]
	 */
	public function postPermissionResource(Request $request)
	{
		$routes = $request->input('routes');
		$id =  $request->input('id');
		$permissionLevelId = $id;
		\App\PermissionResource::resources($permissionLevelId)->delete();

		if($routes){
			foreach ($routes as $route) {
			$routeData = explode('->', $route);
			\App\PermissionResource::create([
					'uri' => $routeData[1],
					'method' => $routeData[0],
					'method_uri' => $route,
					'permission_code' => $permissionLevelId
				]);
			}
		}
		$routePermissions = \App\PermissionResource::resources($permissionLevelId)->lists('uri','method');

		echo json_encode(array('success'=>$routePermissions));
	}

	/**
	 * [show list of open routes]
	 * @return [view] [show a table showing all open routes]
	 */
	public function getOpenRoutes()
	{
		$openRoutes = \App\OpenRoute::all();
		$page_title = 'Open Routes';

		return view('permissions.open-routes', compact('openRoutes','page_title'));
	}

	/**
	 * [show list of open routes]
	 * @return [view] [show a table showing all open routes]
	 */
	public function createOpenRoutes()
	{
		$routes = \Route::getRoutes();
		$openRoutes = \App\OpenRoute::lists('route');
		$page_title = 'Open Routes';

		return view('permissions.create-open-route', compact('openRoutes','page_title', 'routes'));
	}

	/**
	 * Store a newly created open route in storage.
	 *
	 * @return Response
	 */
	public function storeOpenRoutes(Request $request)
	{
		$validator = Validator::make(
			[
				'route' => $request->input('route'),
				'method' => $request->input('method')
			],
		 	[
		 		'route' => 'required|max:255',
		 		'method' => 'required|max:255'
			]);

		if ( $validator->fails() ) {
			$this->throwValidationException(
				$request, $validator
			);
		}

		$openRoute= new \App\OpenRoute();
		$openRoute->route = $request->input('route');
		$openRoute->method= $request->input('method');
		$openRoute->save();

		$openRoutesAll = \App\OpenRoute::all();

		echo json_encode(array('success'=>$openRoutesAll));
	}

	/**
	 * Delete open route.
	 *
	 * @return Response json form
	 */
	public function deleteOpenRoutes(Request $request)
	{
	   $id = $request->input('id');
	   \App\OpenRoute::find($id)->delete();
	   $openRoutesAll = \App\OpenRoute::all();

	   echo json_encode(array('success'=>$openRoutesAll));

	}

	/**
	 * [show list of view elements for a permission Id]
	 * @param  [integer] $permissionId [description]
	 * @return [view]               [description]
	 */
	public function getPermissionViewElements(Request $request)
	{
		//$page_title = 'View Elements';
		$permissionId = $request->input('id');
		//$viewElements = \App\ViewElement::orderBy('element_group')->get();
		$permissionViewElements = \App\PermissionViewElement::elements($permissionId)->lists('view-element_id');
		/*$userElements = [];
		$elementGroup = null;
		foreach (\Auth::user()->permission->elements as $element) {
			$userElements[] = $element->element->element_name;
		}*/
        echo json_encode(array('success'=>$permissionViewElements));
		//return view('permissions.permission-view-elements', compact('page_title', 'viewElements', 'permissionViewElements', 'permissionId', 'elementGroup'));
	}


	/**
	 * [ save view elements for a permission Id]
	 * @param  [integer] $permissionId [description]
	 * @return [redirection]               [redirect to view elements]
	 */
	public function postPermissionViewElements(Request $request)
	{

		$permissionId = $request->input('id');
		$elements = $request->input('elements');
		\App\PermissionViewElement::elements($permissionId)->delete();

		if($elements){
			foreach ($elements as $element) {
			$permission = new \App\PermissionViewElement();
			$permission->{'view-element_id'} = $element;
			$permission->permission_code = $permissionId;
			$permission->save();
			}
		}

		echo json_encode(array('success'=>200));
	}

}

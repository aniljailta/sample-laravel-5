<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//to listen all query executions
DB::listen(function($squery, $params, $time)
{
  // dd(array($squery, $params, $time));
});
Route::get('/', 'HomeController@logined');

Route::get('not-active',['as' => 'not-active','uses' => 'HomeController@notActive']);

Route::group(['middleware' => ['Isactive','auth.route']], function()
{
		Route::get('home/quotes', ['as' => 'quotes-for-calender', 'uses' => 'HomeController@getQuotesForCal']);

		Route::get('home', ['as' => 'home-page', 'uses' => 'HomeController@index']);

		Route::get('quotes', ['as' => 'quotes-page', 'uses' => 'QuotesController@index']);

		
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('sandbox', function(){
	var_dump(Request::getUserInfo());
	var_dump(Request::method());
	dd(Route::getRoutes());
});

Route::get('un-authorized', 'HomeController@unAuthorized');

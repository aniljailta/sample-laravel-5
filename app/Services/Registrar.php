<?php namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{ 
	
	  return Validator::make($data, [
			'first-name' => 'required|max:255',
			'last-name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:backend-user',
			'password' => 'required|confirmed|min:6',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		return User::create([
			'FirstName' => $data['first-name'],
			'LastName' => $data['last-name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
			'Phone1' => $data['phone'],
			'IsActive' => 'Y',  // 'Y','N'
			'Level' => 'Administrator', // 'User','Administrator'
		]);
	}

}

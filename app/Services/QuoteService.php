<?php

namespace App\Services;

use DB;
use App\Quote;
use App\Mover;
use App\Owner;

class QuoteService
{

	public function getQuoteList()
	{
		return Quote::JoinCarrier()->JoinOwner()
			->select('quote.quote_id', 'quote.requested_load_date', 'quote.load_city', 'quote.unload_city','quote.quote_file_name',
				'owner.owner_id', 'owner.first_name', 'owner.last_name',
				'carrier.carrier_id', 'carrier.company_web_name','carrier.server_base_url',
				'quote.agreement_signature_date')
			->OrderRequestedLoadDate()
			->get();
	}

	public function getBookedMoveTodayNoti(){

	    $today = date('Y-m-d');
		$date=strtotime($today);
		//$newDate = date('Y-m-d',strtotime('+10 days',$date));
		//$yesterday = date('Y-m-d',strtotime('-1 days',$date));
		$newDate = \Carbon\Carbon::now()->addDays(10);
		$yesterday = \Carbon\Carbon::now()->subDay();
		$quotes =  Quote::JoinCarrier()->JoinOwner()
			->select('quote.quote_id', 'quote.created_at','quote.requested_load_date', 'quote.load_city', 'quote.unload_city','quote.quote_file_name',
				'owner.owner_id', 'owner.first_name', 'owner.last_name',
				'carrier.carrier_id', 'carrier.company_web_name','carrier.server_base_url',
				'quote.agreement_signature_date')
		     ->where('agreement_signature_date',$today)->orwhere('agreement_signature_date',$yesterday)
			 ->OrderRequestedLoadDate()->get();

	}

	public function getQuote($quote_id)
	{
		return Quote::find($quote_id);
	}

	public function getBookedMove()
	{
		return Quote::JoinCarrier()->JoinOwner()
			->select('quote.quote_id', 'quote.requested_load_date', 'quote.load_city', 'quote.unload_city',
				'owner.owner_id', 'owner.first_name', 'owner.last_name',
				'carrier.carrier_id', 'carrier.company_web_name',
				'quote.agreement_signature_date')
			->AgreementSignatureDate()->get();
	}

	public function getBookedMoveToday($type)
	{
	    $today = date('Y-m-d');
		$date=strtotime($today);
		$newDate = \Carbon\Carbon::now()->addDays(10);
		$yesterday = \Carbon\Carbon::now()->subDay();
		//$yesterday = date('Y-m-d',strtotime('-1 days',$date));
		if($type == 'all'){
		  $quotes =  Quote::JoinCarrier()->JoinOwner()
			->select('quote.quote_id', 'quote.created_at','quote.requested_load_date', 'quote.load_city', 'quote.unload_city','quote.quote_file_name',
				'owner.owner_id', 'owner.first_name', 'owner.last_name',
				'carrier.carrier_id', 'carrier.company_web_name','carrier.server_base_url',
				'quote.agreement_signature_date')
		    //->where('created_at',$today)->orwhere('created_at',$yesterday)
			  ->OrderRequestedLoadDate()->get();
		}else{
		 $quotes =  Quote::JoinCarrier()->JoinOwner()
			->select('quote.quote_id','quote.created_at', 'quote.requested_load_date', 'quote.load_city', 'quote.unload_city','quote.quote_file_name',
				'owner.owner_id', 'owner.first_name', 'owner.last_name',
				'carrier.carrier_id', 'carrier.company_web_name','carrier.server_base_url',
				'quote.agreement_signature_date')
		    ->where('requested_load_date','>',$yesterday)
			->OrderRequestedLoadDate()->get();
		}
		return $quotes;
	}

	public function getMover($mover_id)
	{
		return Carrier::find($mover_id);
	}

	public function getOwner($shipper_id)
	{
		return Owner::find($shipper_id);
	}
}
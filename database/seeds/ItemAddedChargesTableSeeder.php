<?php

use Illuminate\Database\Seeder;
use App\ItemAddedCharge;

class ItemAddedChargeTableSeeder extends Seeder {
    public function run()
    {
        DB::table('item-added-charges')->delete();
        $json = File::get("./database/data/item-added-charges.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          ItemAddedCharge::create(array(
            'item-added-charges_id' =>  $obj->{'item-added-charges_id'},
            'item_id' => $obj->item_id,
            'added_charge_type' =>  $obj->added_charge_type,
            'packing_label'   =>  $obj->packing_label,
            'packing_charge' =>  $obj->packing_charge,
            'unpacking_label'    =>  $obj->unpacking_label,
            'unpacking_charge'    =>  $obj->unpacking_charge,
            'take_apart_label'   =>  $obj->take_apart_label,
            'take_apart_charge' =>  $obj->take_apart_charge,
            'last_edited_by'    =>  $obj->last_edited_by
          ));
        }
    }
}
?>
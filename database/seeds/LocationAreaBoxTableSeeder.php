<?php

use Illuminate\Database\Seeder;
use App\LocationAreaBox;

class LocationAreaBoxTableSeeder extends Seeder {
    public function run()
    {
        DB::table('location_area_box')->delete();
        $json = File::get("./database/data/location_area_box.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          LocationAreaBox::create(array(
            'carrier_id' => $obj->carrier_id,
            'location_id' =>  $obj->location_id,
            'area_id'   =>  $obj->area_id,
            'item_id' =>  $obj->item_id,
            'box_count'    =>  $obj->box_count,
            'last_edited_by'    =>  $obj->last_edited_by
          ));
        }
    }
}
?>
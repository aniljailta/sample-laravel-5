<?php

use Illuminate\Database\Seeder;
use App\PermissionLevel;

class PermissionLevelTableSeeder extends Seeder {
    public function run()
    {
        DB::table('permission-level')->delete();
        $json = File::get("database/data/permission-level.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          PermissionLevel::create(array(
            'permission-level_id'   =>  $obj->{'permission-level_id'},
            'permission_code' => $obj->permission_code,
            'permission_label' => $obj->permission_label,
            'permission_type' => $obj->permission_type,
            'last_edited_by' => $obj->last_edited_by
          ));
        }
    }
}
?>
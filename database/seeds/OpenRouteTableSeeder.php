<?php

use Illuminate\Database\Seeder;
use App\OpenRoute;

class OpenRouteTableSeeder extends Seeder {
    public function run()
    {
        DB::table('open-route')->delete();
        $json = File::get("./database/data/open-route.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          OpenRoute::create(array(
            'open-route_id' =>  $obj->{'open-route_id'},
            'route' =>  $obj->route,
            'method'    =>  $obj->method,
            'last_edited_by'    =>  $obj->last_edited_by
          ));
        }
    }
}
?>
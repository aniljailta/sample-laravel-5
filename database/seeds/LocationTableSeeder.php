<?php

use Illuminate\Database\Seeder;
use App\Location;

class LocationTableSeeder extends Seeder {
    public function run()
    {
        DB::table('location')->delete();
        $json = File::get("./database/data/location.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          Location::create(array(
            'location_id' =>  $obj->location_id,
            'carrier_id'    =>  $obj->carrier_id,
            'location_type'  =>  $obj->location_type,
            'last_edited_by'    =>  $obj->last_edited_by
          ));
        }
    }
}
?>
<?php

use Illuminate\Database\Seeder;
use App\LocalZipCode;

class LocalZipCodeTableSeeder extends Seeder {
    public function run()
    {
        DB::table('local-zip-code')->delete();
        $json = File::get("./database/data/local-zip-code.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          LocalZipCode::create(array(
            'local-zip-code_id' =>  $obj->{'local-zip-code_id'},
            'carrier_id'    =>  $obj->carrier_id,
            'zip_code'  =>  $obj->zip_code,
            'county_name'   =>  $obj->county_name,
            'last_edited_by'    =>  $obj->last_edited_by
          ));
        }
    }
}
?>
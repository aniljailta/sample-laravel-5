<?php

use Illuminate\Database\Seeder;
use App\MovingRate;

class MovingRateTableSeeder extends Seeder {
    public function run()
    {
        DB::table('moving-rate')->delete();
        $json = File::get("./database/data/moving-rate.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          MovingRate::create(array(
            'moving-rate_id' =>  $obj->{'moving-rate_id'},
            'carrier_id'    =>  $obj->carrier_id,
            'man_working_count'  =>  $obj->man_working_count,
            'moving_rate_per_hour'  =>  $obj->moving_rate_per_hour,
            'last_edited_by'    =>  $obj->last_edited_by
          ));
        }
    }
}
?>
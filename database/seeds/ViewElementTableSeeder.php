<?php

use Illuminate\Database\Seeder;
use App\ViewElement;

class ViewElementTableSeeder extends Seeder {
    public function run()
    {
        DB::table('view-element')->delete();
        $json = File::get("./database/data/view-element.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          ViewElement::create(array(
            'view-element_id' => $obj->{'view-element_id'},
            'element_group'    =>  $obj->element_group,
            'element_name'    =>  $obj->element_name,
            'description'   =>  $obj->description,
            'last_edited_by'    =>  $obj->last_edited_by
          ));
        }
    }
}
?>
<?php

use Illuminate\Database\Seeder;
use App\User;

class BackendUserTableSeeder extends Seeder {
    public function run()
    {
        DB::table('backend-user')->delete();
        $json = File::get("./database/data/backend-user.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          User::create(array(
            'first_name' => $obj->first_name,
            'last_name' => $obj->last_name,
            'email' => $obj->email,
            'password' => $obj->password,
            'phone1' => $obj->phone1,
            'is_active' => $obj->is_active,
            'permission_code' => $obj->permission_code,
            'carrier_id' => $obj->carrier_id
          ));
        }
    }
}
?>
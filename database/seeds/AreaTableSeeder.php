<?php

use Illuminate\Database\Seeder;
use App\Area;

class AreaTableSeeder extends Seeder {
    public function run()
    {
        DB::table('area')->delete();
        $json = File::get("./database/data/area.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          Area::create(array(
            'area_id'   =>  $obj->area_id,
            'carrier_id' => $obj->carrier_id,
            'area_type' =>  $obj->area_type,
            'last_edited_by'    => $obj->last_edited_by
          ));
        }
    }
}
?>
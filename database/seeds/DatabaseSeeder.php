<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('PermissionLevelTableSeeder');
        $this->command->info('permission-level table seeded!');

        $this->call('CarrierTableSeeder');
        $this->command->info('carrier table seeded!');

        $this->call('AreaTableSeeder');
        $this->command->info('area table seeded!');

        $this->call('ItemTableSeeder');
        $this->command->info('item table seeded!');

        $this->call('LocationTableSeeder');
        $this->command->info('location table seeded!');

        $this->call('AreaItemTableSeeder');
        $this->command->info('area_item table seeded!');

        $this->call('BackendUserTableSeeder');
        $this->command->info('backend-user table seeded!');

        $this->call('ItemAddedChargeTableSeeder');
        $this->command->info('item-added-charges table seeded!');

        $this->call('LocalZipCodeTableSeeder');
        $this->command->info('local-zip-code table seeded');

        $this->call('LocationAreaTableSeeder');
        $this->command->info('location_area table seeded!');

        $this->call('LocationAreaBoxTableSeeder');
        $this->command->info('location_area_box table seeded!');

        $this->call('MovingRateTableSeeder');
        $this->command->info('moving-rate table seeder');

        $this->call('OpenRouteTableSeeder');
        $this->command->info('open-route table seeded!');

        $this->call('StateTableSeeder');
        $this->command->info('state table seeded!');

        $this->call('ViewElementTableSeeder');
        $this->command->info('view-element table seeded!');


	}

}

<?php

use Illuminate\Database\Seeder;
use App\State;

class StateTableSeeder extends Seeder {
    public function run()
    {
        DB::table('state')->delete();
        $json = File::get("./database/data/state.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          State::create(array(
            'state_id' => $obj->state_id,
            'state_code'    =>  $obj->state_code,
            'state_name'    =>  $obj->state_name,
            'last_edited_by'    =>  $obj->last_edited_by
          ));
        }
    }
}
?>
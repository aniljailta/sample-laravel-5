<?php

use Illuminate\Database\Seeder;
use App\LocationArea;

class LocationAreaTableSeeder extends Seeder {
    public function run()
    {
        DB::table('location_area')->delete();
        $json = File::get("./database/data/location_area.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          LocationArea::create(array(
            'location_area_id' => $obj->location_area_id,
            'location_id'    =>  $obj->location_id,
            'carrier_id'    =>  $obj->carrier_id,
            'area_id'   =>  $obj->area_id,
            'last_edited_by'    =>  $obj->last_edited_by
          ));
        }
    }
}
?>
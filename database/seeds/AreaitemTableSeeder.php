<?php

use Illuminate\Database\Seeder;
use App\AreaItem;

class AreaItemTableSeeder extends Seeder {
    public function run()
    {
        DB::table('area_item')->delete();
        $json = File::get("./database/data/area_item.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          AreaItem::create(array(
            'carrier_id' => $obj->carrier_id,
            'area_id'   =>  $obj->area_id,
            'item_id'   =>  $obj->item_id,
            'item_count'    =>  $obj->item_count,
            'last_edited_by'    =>  $obj->last_edited_by
          ));
        }
    }
}
?>
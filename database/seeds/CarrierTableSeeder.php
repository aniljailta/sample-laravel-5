<?php

use Illuminate\Database\Seeder;
use App\Carrier;

class CarrierTableSeeder extends Seeder {
    public function run()
    {
        DB::table('carrier')->delete();
        Carrier::create(array(
            'carrier_id'    =>  0,
            'company_web_name' => 'Default Company',
            'last_edited_by' =>  0
        ));

        Carrier::where('carrier_id',1)->update(['carrier_id' => 0]);

        $json = File::get("./database/data/carrier.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          Carrier::create(array(
            'carrier_id'    =>  $obj->carrier_id,
            'company_web_name' => $obj->company_web_name,
            'company_name_other'    =>  $obj->company_name_other,
            'tagline'   =>  $obj->tagline,
            'street_address'    =>  $obj->street_address,
            'city'  =>  $obj->city,
            'state' =>  $obj->state,
            'zip'   =>  $obj->zip,
            'email' =>  $obj->email,
            'phone1' =>  $obj->phone1,
            'phone2' =>  $obj->phone2,
            'phone3' =>  $obj->phone3,
            'license_number1' =>  $obj->license_number1,
            'registration_number1' =>  $obj->registration_number1,
            'license_number2' =>  $obj->license_number2,
            'registration_number2' =>  $obj->registration_number2,
            'latitude' =>  $obj->latitude,
            'longtitude' =>  $obj->longtitude,
            'website_url' =>  $obj->website_url,
            'server_base_url' =>  $obj->server_base_url,
            'logo' =>  $obj->logo,
            'banner' =>  $obj->banner,
            'man_hours_numerator' =>  $obj->man_hours_numerator,
            'man_hours_denominator' =>  $obj->man_hours_denominator,
            'max_hours_per_day' =>  $obj->max_hours_per_day,
            'maximum_weight_per_truck' =>  $obj->maximum_weight_per_truck,
            'percent_increase_elevators' =>  $obj->percent_increase_elevators,
            'percent_increase_stairs' =>  $obj->percent_increase_stairs,
            'percent_increase_first_flight' =>  $obj->percent_increase_first_flight,
            'percent_increase_added_flights' =>  $obj->percent_increase_added_flights,
            'percent_increase_per_foot_walking' =>  $obj->percent_increase_per_foot_walking,
            'walking_distance_increase_start_point' =>  $obj->walking_distance_increase_start_point,
            'travel_speed_per_hour' =>  $obj->travel_speed_per_hour,
            'fuel_miles_per_gallon' =>  $obj->fuel_miles_per_gallon,
            'fuel_charge_per_gallon' =>  $obj->fuel_charge_per_gallon,
            'truck_fee_per_truck' =>  $obj->truck_fee_per_truck,
            'last_edited_by' =>  $obj->last_edited_by
          ));
        }
    }
}
?>
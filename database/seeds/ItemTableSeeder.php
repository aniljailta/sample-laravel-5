<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemTableSeeder extends Seeder {
    public function run()
    {
        DB::table('item')->delete();
        DB::unprepared(File::get('./database/data/item.sql'));
    }
}
?>
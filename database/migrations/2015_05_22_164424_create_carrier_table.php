<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrierTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carrier', function(Blueprint $table){

			$table->engine = 'InnoDB';
			$table->increments('carrier_id')->unsigned();
			$table->string('company_web_name',50)->default('');
			$table->string('company_name_other',50)->default('');
			$table->string('tagline',50)->default('');
			$table->string('street_address',75)->default('');
			$table->string('city',30)->default('');
			$table->string('state',10)->default('');
			$table->string('zip',15)->default('');
			$table->string('email',100)->default('');
			$table->string('phone1',15)->default('');
			$table->string('phone2',15)->default('');
			$table->string('phone3',15)->default('');
			$table->string('license_number1',15)->default('');
			$table->string('registration_number1',15)->default('');
			$table->string('license_number2',15)->default('');
			$table->string('registration_number2',15)->default('');
			$table->decimal('latitude',13,10)->default(0.0000);
			$table->decimal('longtitude',13,10)->default(0.0000);
			$table->string('website_url',100)->default('');
			$table->string('server_base_url',100)->default('');
			$table->string('logo',80)->default('');
			$table->string('banner',80)->default('');
			$table->smallInteger('man_hours_numerator')->unsigned()->default(0);
			$table->smallInteger('man_hours_denominator')->unsigned()->default(0);
			$table->decimal('max_hours_per_day',4,2)->unsigned()->default(0.00);
			$table->integer('maximum_weight_per_truck')->unsigned()->default(0);
			$table->decimal('percent_increase_elevators',4,2)->unsigned()->default(0.00);
			$table->decimal('percent_increase_stairs',4,2)->unsigned()->default(0.00);
			$table->decimal('percent_increase_first_flight',4,2)->unsigned()->default(0.00);
			$table->decimal('percent_increase_added_flights',4,2)->unsigned()->default(0.00);
			$table->decimal('percent_increase_per_foot_walking',4,2)->unsigned()->default(0.00);
			$table->smallInteger('walking_distance_increase_start_point')->unsigned()->default(0);
			$table->smallInteger('travel_speed_per_hour')->unsigned()->default(0);
			$table->smallInteger('fuel_miles_per_gallon')->unsigned()->default(0);
			$table->decimal('fuel_charge_per_gallon',5,2)->unsigned()->default(0.00);
			$table->decimal('truck_fee_per_truck',6,2)->unsigned()->default(0.00);
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carrier');
	}

}

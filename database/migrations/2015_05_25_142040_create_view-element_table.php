<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewElementTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('view-element', function(Blueprint $table){

			$table->increments('view-element_id');
			$table->string('element_group',70)->default('');
			$table->string('element_name',70)->default('');
			$table->text('description')->nullable();
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('view-element');
	}

}

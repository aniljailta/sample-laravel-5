<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('offer', function(Blueprint $table){

			$table->increments('offer_id')->unsigned();
			$table->integer('quote_id')->unsigned();
			$table->enum('offer_source',['quote','communication'])->default('quote');
			$table->string('offer_type',50)->default('');
			$table->decimal('offer_amount',7,2)->unsigned()->default(0.00);
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();

			$table->foreign('quote_id')->references('quote_id')->on('quote')
                ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('offer');
	}

}

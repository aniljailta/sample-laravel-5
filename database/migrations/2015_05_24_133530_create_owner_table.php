<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnerTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('owner', function(Blueprint $table){

			$table->engine = "InnoDB";
			$table->increments('owner_id')->unsigned();
			$table->string('temporary_pasword', 255)->default('')->comment('use for activation');
			$table->string('password',255)->deault('')->comment('use for login');
			$table->string('first_name',25)->default('');
			$table->string('last_name',30)->default('');
			$table->string('phone1',25)->default("");
			$table->string('phone2',25)->default("");
			$table->string('email',75)->unique()->comment("user for login");
			$table->string('verification_code',150)->default("");
			$table->dateTime('verification_deadline_date')->nullable();
			$table->enum('is_verified', ['No','Yes'])->default('No');
			$table->dateTime('verification_date')->nullable();
			$table->enum('is_active', ['No','Yes'])->default('No');
			$table->dateTime('last_login_attempt')->nullable();
			$table->smallInteger('login_attempt_count')->unsigned()->default(0);
			$table->smallInteger('login_attempt_limit')->unsigned()->default(4);
			$table->smallInteger('lockout_duration')->unsigned()->default(60);
			$table->dateTime('last_used')->nullable();
			$table->smallInteger('login_count')->unsigned()->default(0);
			$table->dateTime('reset_request_date')->nullable();
			$table->smallInteger('reset_request_count')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->string('remember_toke',100)->default('');
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('owner');
	}

}

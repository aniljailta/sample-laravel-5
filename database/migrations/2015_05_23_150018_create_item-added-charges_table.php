<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemAddedChargesTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item-added-charges', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('item-added-charges_id')->unsigned();
			$table->integer('item_id')->unsigned()->index();
			$table->smallInteger('added_charge_type')->unsigned()->default(0);
			$table->string('packing_label',20)->default("");
			$table->decimal('packing_charge', 7, 2)->unsigned()->default(0.00);
			$table->string('unpacking_label',20)->default("");
			$table->decimal('unpacking_charge', 7, 2)->unsigned()->default(0.00);
			$table->string('take_apart_label',20)->default("");
			$table->decimal('take_apart_charge', 7, 2)->unsigned()->default(0.00);
			$table->string('put_together_label',20)->default("");
			$table->decimal('put_together_charge', 7, 2)->unsigned()->default(0.00);
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();

			$table->foreign('item_id')->references('item_id')->on('item')
                ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item-added-charges');
	}

}

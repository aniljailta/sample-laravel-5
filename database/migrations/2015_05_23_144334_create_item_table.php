<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('item_id')->unsigned();
			$table->integer('carrier_id')->unsigned()->index();
			$table->string('item_type',100)->default('');
			$table->string('short_label',80)->default('');
			$table->string('spanish_label',80)->default('');
			$table->string('long_label',160)->default('');
			$table->integer('weight')->default(0);
			$table->string('image_file_name',120)->default('');
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();

			$table->foreign('carrier_id')->references('carrier_id')->on('carrier')
                ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item');
	}

}

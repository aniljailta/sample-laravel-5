<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionLevelTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permission-level', function(Blueprint $table){

			$table->engine = 'InnoDB';
			$table->increments('permission-level_id')->unsigned();
			$table->integer('permission_code')->unsigned()->default(0)->unique()->comment('used to classify different permission levels within a category');
			$table->string('permission_label',100)->default('');
			$table->enum('permission_type',['company','general'])->default('company');
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permission-level');
	}

}

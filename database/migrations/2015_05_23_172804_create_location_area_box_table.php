<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationAreaBoxTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('location_area_box', function(Blueprint $table){

			$table->engine = 'InnoDB';
			$table->increments('location_area_box_id')->unsigned();
			$table->integer('carrier_id')->unsigned()->index();
			$table->integer('location_id')->unsigned()->inex();
			$table->integer('area_id')->unsigned()->index();
			$table->integer('item_id')->unsigned()->index();
			$table->smallInteger('box_count')->unsigned();
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();

			$table->foreign('location_id')->references('location_id')->on('location')
			    ->onUpdate('cascade');
			$table->foreign('carrier_id')->references('carrier_id')->on('carrier')
                ->onUpdate('cascade');
            $table->foreign('area_id')->references('area_id')->on('area')
                ->onUpdate('cascade');
            $table->foreign('item_id')->references('item_id')->on('item')
                ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('location_area_box');
	}

}

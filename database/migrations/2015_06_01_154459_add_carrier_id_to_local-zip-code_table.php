<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarrierIdToLocalZipCodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('local-zip-code', function(Blueprint $table){

			$table->integer('carrier_id')->unsigned()->after('local-zip-code_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('local-zip-code', function(Blueprint $table){

			$table->dropColumn('carrier_id');
		});
	}

}

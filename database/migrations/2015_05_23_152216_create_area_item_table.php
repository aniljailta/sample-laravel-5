<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaItemTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('area_item', function(Blueprint $table){

			$table->engine = 'InnoDB';
			$table->increments('area_item_id')->unsigned();
			$table->integer('carrier_id')->unsigned()->index();
			$table->integer('area_id')->unsigned()->index();
			$table->integer('item_id')->unsigned()->index();
			$table->smallInteger('item_count')->unsigned()->default(1);
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();

			$table->foreign('carrier_id')->references('carrier_id')->on('carrier')
                ->onUpdate('cascade');
            $table->foreign('area_id')->references('area_id')->on('area')
                ->onUpdate('cascade');
            $table->foreign('item_id')->references('item_id')->on('item')
                ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('area_item');
	}

}

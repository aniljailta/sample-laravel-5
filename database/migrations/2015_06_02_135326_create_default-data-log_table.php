<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultDataLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('default-data-log', function(Blueprint $table){

			$table->engine = 'MyISAM';
			$table->increments('default-data-log_id')->unsigned();
			$table->integer('carrier_id')->unsigned()->default();
			$table->integer('flag')->unsigned()->default();
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('default-data-log');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTempTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('location-temp', function(Blueprint $table){

			$table->engine = 'MyISAM';
			$table->increments('location-temp_id')->unsigned();
			$table->integer('default_location_id')->unsigned()->default(0);
			$table->integer('new_location_id')->unsigned()->default();
			$table->integer('carrier_id')->unsigned()->default();
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('location-temp');
	}

}

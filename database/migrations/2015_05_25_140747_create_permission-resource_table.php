<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionResourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permission-resource', function(Blueprint $table){

			$table->increments('permission-resource_id');
			$table->string('method',15)->default('');
			$table->string('uri',100)->default('');
			$table->string('method_uri',115)->default('');
			$table->integer('permission_code')->unsigned()->index();
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();

			$table->foreign('permission_code')->references('permission_code')->on('permission-level')
                ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permission-resource');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationAreaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('location_area', function(Blueprint $table){

			$table->increments('location_area_id')->unsigned();
			$table->integer('location_id')->unsigned();
			$table->integer('carrier_id')->unsigned();
			$table->integer('area_id')->unsigned();
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();

			$table->foreign('carrier_id')->references('carrier_id')->on('carrier')
                ->onUpdate('cascade');
            $table->foreign('area_id')->references('area_id')->on('area')
                ->onUpdate('cascade');
            $table->foreign('location_id')->references('location_id')->on('location')
                ->onUpdate('cascade');
            $table->index(array('carrier_id','location_id','area_id'));
            $table->index(array('carrier_id','area_id','location_id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}

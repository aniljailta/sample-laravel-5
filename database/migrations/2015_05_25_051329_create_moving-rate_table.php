<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovingRateTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('moving-rate', function(Blueprint $table){

			$table->engine = 'InnoDB';
			$table->increments('moving-rate_id');
			$table->integer('carrier_id')->unsigned()->default(0)->index()->comment("foreign key from carrier table");
			$table->smallInteger('man_working_count')->unsigned()->default(0);
			$table->decimal('moving_rate_per_hour',7,2)->unsigned()->default(0.00);
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();

			$table->foreign('carrier_id')->references('carrier_id')->on('carrier')
                ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}

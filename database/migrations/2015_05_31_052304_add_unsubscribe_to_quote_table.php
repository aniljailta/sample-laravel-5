<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnsubscribeToQuoteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('quote', function(Blueprint $table){

			$table->integer('unsubscribe')->unsigned()->default(0)->after('quote_amount');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('quote', function(Blueprint $table){

			$table->dropColumn('unsubscribe');
		});
	}

}

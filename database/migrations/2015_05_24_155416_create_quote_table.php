<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('quote', function(Blueprint $table){

			$table->engine = 'InnoDB';
			$table->increments('quote_id')->unsigned();
			$table->integer('owner_id')->unsigned()->default(0)->index();
			$table->integer('carrier_id')->unsigned()->default(0)->index();
			$table->dateTime('estimate_date')->nullable();
			$table->date('requested_load_date')->nullable();
			$table->date('requested_unload_date')->nullable();
			$table->string('load_street',50)->default('');
			$table->string('load_unit_number',15)->default('');
			$table->string('load_city',25)->default('');
			$table->string('load_state',15)->default('');
			$table->string('load_zip',15)->default('');
			$table->string('unload_street',50)->default('');
			$table->string('unload_unit_number',15)->default('');
			$table->string('unload_city',25)->default('');
			$table->string('unload_state',15)->default('');
			$table->string('unload_zip',15)->default('');
			$table->smallInteger('additional_stop_count')->unsigned()->default(0);
			$table->enum('temp_storage',['Yes','No'])->default('No');
			$table->smallInteger('item_count')->unsigned()->default(0);
			$table->smallInteger('box_count')->unsigned()->default(0);
			$table->integer('total_weight')->unsigned()->default(0);
			$table->integer('travel_distance')->unsigned()->default(0);
			$table->binary('quote_array');
			$table->string('quote_file_name',30)->default('');
			$table->decimal('quote_amount',7,2)->unsigned()->default(0.00);
			$table->decimal('fee_amount_paid',7,2)->unsigned()->default(0.00);
			$table->dateTime('date_fee_paid')->nullable();
			$table->string('fee_transaction_id',50)->default('');
			$table->string('agreement_signature',150)->default('');
			$table->dateTime('agreement_signature_date')->nullable();
			$table->time('load_start_time_restriction')->nullable();
			$table->time('load_stop_time_restriction')->nullable();
			$table->time('unload_start_time_restriction')->nullable();
			$table->time('unload_stop_time_restriction')->nullable();
			$table->date('scheduled_move_date')->nullable();
			$table->date('actual_move_date')->nullable();
			$table->time('actual_move_start_time')->nullable();
			$table->smallInteger('truck_count')->unsigned()->default(0);
			$table->smallInteger('man_count')->unsigned()->default(0);
			$table->enum('move_completed',['Yes','No'])->default('No');
			$table->string('move_rating',100)->default('');
			$table->enum('test_quote',['Yes','No'])->default('No');
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();

			$table->foreign('owner_id')->references('owner_id')->on('owner')
                ->onUpdate('cascade');
            $table->foreign('carrier_id')->references('carrier_id')->on('carrier')
                ->onUpdate('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}

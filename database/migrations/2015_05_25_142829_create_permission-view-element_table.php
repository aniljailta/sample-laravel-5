<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionViewElementTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permission-view-element', function(Blueprint $table){

			$table->increments('permission-view-element_id');
			$table->integer('permission_code')->unsigned()->index();
			$table->integer('view-element_id')->unsigned()->index();
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();

			$table->foreign('permission_code')->references('permission_code')->on('permission-level')
                ->onUpdate('cascade');
            $table->foreign('view-element_id')->references('view-element_id')->on('view-element')
                ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permission-view-element');
	}

}

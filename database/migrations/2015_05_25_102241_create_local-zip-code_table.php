<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateLocalZipCodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('local-zip-code', function(Blueprint $table){

			$table->engine = 'MyISAM';
			$table->increments('local-zip-code_id')->unsigned();
			$table->char('zip_code',5)->default('')->unique();
			$table->string('county_name',30)->default('');
			$table->integer('last_edited_by')->unsigned()->default(0);
			$table->nullableTimestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('local-zip-code');
	}

}

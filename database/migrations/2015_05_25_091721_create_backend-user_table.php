<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackendUserTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('backend-user', function(Blueprint $table){

			$table->engine = 'InnoDB';
			$table->increments('backend-user_id')->unsigned();
			$table->integer('carrier_id')->unsigned()->default(0)->index()->comment('foreign key to carrier table');
			$table->string('first_name',25)->default('');
			$table->string('last_name',30)->default('');
			$table->string('password',255)->default('')->comment('used for login');
			$table->string('phone1',25)->default('');
			$table->string('email',75)->default('')->comment('used for login');
			$table->enum('is_active',['Yes','No'])->default('No');
			$table->integer('permission_code')->unsigned()->nullable()->comment('foreign key to permission level');
			$table->dateTime('current_login_date')->nullable()->comment('this record needs to be kept separate and based on logins, not just edits to the record');
			$table->dateTime('last_login_date')->nullable();
			$table->integer('last_edited_by')->unsigned()->nullable()->comment('FK to itself');
			$table->nullableTimestamps();
			$table->string('remember_token',100)->default('');
			$table->softDeletes();

			$table->foreign('carrier_id')->references('carrier_id')->on('carrier')
                ->onUpdate('cascade');
            $table->foreign('permission_code')->references('permission_code')->on('permission-level')
                ->onUpdate('cascade');
            $table->foreign('last_edited_by')->references('backend-user_id')->on('backend-user')
                ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}

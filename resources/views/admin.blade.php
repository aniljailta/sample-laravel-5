@extends('app')

@section('content')
<div class="pagetitle">
	<h1>Administrative Settings.</h1>
</div><!--pagetitle-->

@include('permission.modals')

<div class="maincontent">
	<div class="contentinner content-dashboard">	
		<div class="widgetcontent">
			<div id="tabs-content" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
				<ul>
					<li class="tab" id="Permissions"><a href="#tabs-1" ><span class=""></span>Permissions</a></li>
				</ul>
			
			 <div id="tabs-1">

	          <div class="clearfix"></div>			  
			   <div id="routes-content" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
			      <ul>
				        <li class="tab" id="routes"><a href="#permission_list" ><span class=""></span>List / Routes</a></li>
						<li class="tab"><a href="#tabs-2" ><span class=""></span>Open Routes</a></li>
						<li class="tab"><a href="#tabs-3" ><span class=""></span>Components</a></li>
						<li class="tab"><a href="#tabs-4" ><span class=""></span>Carriers</a></li>
			      </ul>
				  <div id="tabs-4">
				    @include('permission.carrier')	
				  </div>
				  <div id="tabs-3">
				     @include('permission.elements')				  
				  </div>
				  <div id="tabs-2">
				     @include('permission.open_route')				  
				  </div>
				   <div id="permission_list">
				     @include('permission.permissionlist')	
					<div class="divider15"></div>
	
					<h4 class="widgettitle">Resources / Routes <span style="float:right;" class="permission_name_details"> name </span></h4>
					
					<div class="widgetcontent">

					<div id="resources-content" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
						<ul>
							<li class="tab" id="resources"><a href="#resourcesall" ><span class=""></span>Resources</a></li>
							<li class="tab"><a href="#components"><span class=""></span>Components</a></li>
						</ul>
						<div id="resourcesall">
							@include('permission.resources')
						</div>			
						<div id="components">
                           @include('permission.components')
						</div>
					</div>
					</div>
				   
	 
				  </div>
				</div> 
			 </div>
		<!--row-fluid-->
      </div>
	  </div>
		 
	</div>
	<!--contentinner-->
</div><!--maincontent-->

<script type="text/javascript" src="/js/permission.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function($){
	$.uniform.restore();
	$('#selectAllResource').click(function () {
        $('.resources').prop('checked', this.checked);
		$.uniform.update();
		var checked = $('#selectAllResource').is(":checked");
		if(checked){
		  $('.labelselectAllResource').html('Unselect All');
		  }else{
		  $('.labelselectAllResource').html('select All');
		}
    });
	
	$('#selectAll').click(function () {
	    console.log('here');
        $('.elements').prop('checked', this.checked);
		$.uniform.update();
		var checked = $('#selectAll').is(":checked");
		if(checked){
		 $('.labelselectAll').html('Unselect All');
		}else{
		  $('.labelselectAll').html('Select All');
		}
    });
	
		
});
</script>

<style>
    /* Example Styles for Demo */
    .etabs { margin: 0; padding: 0; }
    .tab { display: inline-block; zoom:1; *display:inline; background: #eee; border: solid 1px #999; border-bottom: none; -moz-border-radius: 4px 4px 0 0; -webkit-border-radius: 4px 4px 0 0; }
    .tab a { font-size: 14px; line-height: 2em; display: block; padding: 0 10px; outline: none; }
	.tab a:hover{ text-decoration:none !important; }
    .tab.active { background: #f7f7f7; padding-top: 6px; position: relative; top: 1px; border-color: #666; }
    .tab a.active { font-weight: bold; }
    #tabs-content  #all-tabs { background: #f7f7f7; border: solid #666 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px; }
	.all_tabs { background: #f7f7f7; border-top: solid #ADA5A5 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px;}
    #all-tabs { margin-bottom: 10px; }
	#lists-tab{ margin-bottom: 10px; }
	.tab a.active:hover{text-decoration:none !important;}
	.tab a.active{text-decoration:none !important;}
	
	.col-sm-6 {
	  width: 48%;
	  float: left;
	  margin-left: 10px;
	}
  </style>		

@endsection

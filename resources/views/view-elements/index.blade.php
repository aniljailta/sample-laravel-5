@extends('app')

@section('content')


<div class="pagetitle">
	<h1>Add / Manage View Elements</h1> <!--<span>This is a sample description for this page . . .</span>-->
</div><!--pagetitle-->
<div class="maincontent">
	<div class="contentinner">

		<div class="alert alert-info no-display">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	        View element retrived
	    </div>
	    <div class="alert alert-error no-display">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	        Erro occured retriving view element
	    </div>

		<h4 class="widgettitle" id="table_view_elements_title">
			List of View Elements
			<a href="{{url('view-elements/create')}}" class="btn btn-default right" id="">Add New Element</a>
		</h4>


		<table class="table table-bordered" id="table_view_elements">
			<thead>
			<tr>
				<!--<th class="head0 nosort"><input type="checkbox" class="checkall"/></th>-->
				<th width="35%" class="">View Element Name</th>
				<th class=""></th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			@foreach ($viewElements as $element)
			<tr id="{{ $element->id }}">
				<td width="35%"><span id="hexa" class="">{{ $element->description }}</span></td>
				<td><a href="{{url('view-elements/'.$element->id.'/edit')}}" class="btn btn-default" id="">Edit</a></td>
				<td>
					<form action="{{url('view-elements/'.$element->id)}}" method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="_method" value="DELETE">
					<button type="submit" class="btn btn-default">Delete</button>
					</form>
				</td>
			</tr>
			@endforeach

			</tbody>
		</table>

	</div>
		<!--widgetcontent-->
	<!--contentinner-->
</div><!--maincontent-->
@endsection
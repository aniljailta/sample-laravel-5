@extends('app')

@section('content')


<div class="pagetitle">
	<h1>Edit View Element</h1> <!--<span>This is a sample description for this page . . .</span>-->
</div><!--pagetitle-->
<div class="maincontent">
	<div class="contentinner">

		@include('partials.message')
		@include('partials.error')
		<h4 class="widgettitle">
			Edit View Element
		</h4>

		<form action="{{url('view-elements/'.$element->id.'/edit')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="PUT">
		  <!-- <div class="form-group form-element">
		  <label for="elementName">Element Name</label>
		    <input type="text" name="elementName" class="form-control" value="{{$element->element_name}}" id="" placeholder="Enter element name">
		  </div> -->
		  <div class="form-group form-element">
		  <label for="description">Description</label>
		    <textarea name="description" id="" cols="50" rows="10">{{$element->description}}</textarea>
		  </div>
		  <div class="form-group form-element">
		    <button type="submit" class="btn btn-default">Submit</button>
		  </div>

		</form>


	</div>
		<!--widgetcontent-->
	<!--contentinner-->
</div><!--maincontent-->

@endsection
@extends('app')

@section('content')


<div class="pagetitle">
	<h1>Create New View Element</h1> <!--<span>This is a sample description for this page . . .</span>-->
</div><!--pagetitle-->
<div class="maincontent">
	<div class="contentinner">

		@include('partials.message')
		@include('partials.error')
		<h4 class="widgettitle">
			Add New View Element
		</h4>

		<form action="{{url('view-elements')}}" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="form-group form-element">
		  <label for="elementGroup">Element Group</label>
		    <select name="elementGroup" id="">
		    	@foreach($groups as $group)
		    	<option value="{{$group}}">{{$group}}</option>
		    	@endforeach
		    </select>
		    <input type="text" name="newElementGroup" class="form-control" id="" placeholder="Enter new group name">
		  </div>
		  <div class="form-group form-element">
		  <label for="elementName">Element Name</label>
		    <input type="text" name="elementName" class="form-control" id="" placeholder="Enter element name">
		  </div>
		  <div class="form-group form-element">
		  <label for="description">Description</label>
		    <textarea name="description" id="" cols="50" rows="10"></textarea>
		  </div>
		  <div class="form-group form-element">
		    <button type="submit" class="btn btn-default">Submit</button>
		  </div>
		</form>
	</div>
		<!--widgetcontent-->
	<!--contentinner-->
</div><!--maincontent-->

@endsection
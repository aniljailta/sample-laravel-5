<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>
		{{ $page_title or 'Home' }} 
	</title>
	<link rel="stylesheet" href="/css/style.default.css" type="text/css"/>
	<link rel="stylesheet" href="/prettify/prettify.css" type="text/css"/>
	<link rel="stylesheet" href="/css/bootstrap-fileupload.min.css" type="text/css"/>
	<link rel="stylesheet" href="/css/bootstrap-timepicker.min.css" type="text/css"/>
	<link rel="stylesheet" href="/css/custom.style.css" type="text/css"/>

	<script type="text/javascript" src="/prettify/prettify.js"></script>
	<script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/js/jquery-migrate-1.1.1.min.js"></script>
	<script type="text/javascript" src="/js/jquery-ui-1.9.2.min.js"></script>
	<script type="text/javascript" src="/js/jquery.flot.min.js"></script>
	<script type="text/javascript" src="/js/jquery.flot.resize.min.js"></script>
	<script type="text/javascript" src="/js/jquery.flot.pie.js"></script>
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/bootstrap-fileupload.min.js"></script>
	<script type="text/javascript" src="/js/bootstrap-timepicker.min.js"></script>
	<script type="text/javascript" src="/js/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/js/jquery.tagsinput.min.js"></script>
	<script type="text/javascript" src="/js/jquery.autogrow-textarea.js"></script>
	<script type="text/javascript" src="/js/fullcalendar.min.js"></script>
	<script type="text/javascript" src="/js/charCount.js"></script>
	<script type="text/javascript" src="/js/ui.spinner.min.js"></script>
	<script type="text/javascript" src="/js/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="/js/jquery.alerts.js"></script>
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
	<script src="{{URL::to('/')}}/js/jquery.easytabs.min.js" type="text/javascript"></script>
<!--	<script type="text/javascript" src="/js/jquery.media.js"></script>-->
<!--	<script type="text/javascript" src="/js/jquery.metadata.js"></script>-->
	<script type="text/javascript" src="/js/jquery.cookie.js"></script>
	<script type="text/javascript" src="/js/custom.js"></script>
	<script type="text/javascript" src="/js/forms.js"></script>
	 <script src="{{URL::to('/')}}/js/jquery.easytabs.min.js" type="text/javascript"></script>
	<!--[if lte IE 8]>
	<script language="javascript" type="text/javascript" src="/js/excanvas.min.js"></script><![endif]-->
</head>
<style>
.event a {
    background-color: #ffc !important;
    background-image :none !important;
    color: #444 !important;
}
</style>
<body>

<div class="mainwrapper">

	<!-- START OF LEFT PANEL -->
	<div class="leftpanel" style="width:200px;">

		<div class="logopanel">
			<h1><a href="/home"><img src="{{URL::to('img/logo.jpg')}}" ></a></h1>
		</div>
		<!--logopanel-->

		<div class="datewidget">Today is {{ \Carbon\Carbon::now()->toFormattedDateString() }}.</div>

		<div class="leftmenu">
			<ul class="nav nav-tabs nav-stacked">
				<li class="nav-header">Main Navigation</li>

				<!-- Add class="active" based on visible page -->

				 				<!-- Add class="active" based on visible page -->

				@if(canView('Menu_Dashboard_Link'))
 				<li><a href="/home"><span class="icon-align-justify"></span> Dashboard</a></li>
				@endif

				@if(canView('Menu_Quote_List_Link'))
 				<li><a href="/quotes"><span class="icon-list"></span> Quote List</a></li>
				@endif

				@if(canView('Menu_Moving_Schedule_Link'))
 				<li><a href="/schedule"><span class="icon-calendar"></span> Moving Schedule</a></li>
				@endif

 				<!--<li><a href="/statistics"><span class="icon-signal"></span> Statistics</a></li>-->

				@if(canView('Menu_Company_Settings_Link'))
 				<li><a href="/company-settings"><span class="icon-wrench"></span> Company Settings</a></li>
				@endif

 				<!-- adjust labe to "User Account" when user does not have Administrative privileges -->
				@if(canView('Menu_User_Account_Link'))
 				<li><a href="/users"><span class="icon-user"></span> User Accounts</a></li>
				@endif

				@if(canView('Menu_Permission_Level_Link'))
				<li><a href="/admin-setting"><span class="icon-user"></span> Administrative Settings </a></li>
				@endif
			</ul>
		</div>
		<!--leftmenu-->
	</div>
	<!--mainleft-->
	<!-- END OF LEFT PANEL -->


	<!-- START OF RIGHT PANEL -->
	<div class="rightpanel" style="margin-left:200px !important;">
		<div class="headerpanel">
			<a href="" class="showmenu"></a>
				@if(canView('User_Profile_Menu'))
			<div class="headerright">
				<div class="dropdown userinfo">
					<a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="/page.html">Hi, {{
						AUTH::user()->first_name }} {{ AUTH::user()->last_name }}! <b
							class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="/edit_profile"><span class="icon-edit"></span> Edit Profile</a></li>
						<li class="divider"></li>
						<li><a href="/auth/logout"><span class="icon-off"></span> Sign Out</a></li>
					</ul>
				</div>
				<!--dropdown-->

			</div>
			@endif
			<!--headerright-->

		</div>
		<!--headerpanel-->

		<div class="breadcrumbwidget">
			<ul class="skins">
				<!--<li><a href="default" class="skin-color default"></a></li>
				<li><a href="orange" class="skin-color orange"></a></li>
				<li><a href="dark" class="skin-color dark"></a></li>
				<li>&nbsp;</li>-->
				<li class="fixed"><a href="" class="skin-layout fixed"></a></li>
				<li class="wide"><a href="" class="skin-layout wide"></a></li>
			</ul>
			<!--skins-->
			<ul class="breadcrumb">
				<li><a href="{{URL::to('home')}}">Home</a> <span class="divider">/</span></li>
				<li class="active">{{ $page_title }}</li>
			</ul>
		</div>
		<!--breadcrumbwidget-->


		@yield('content')

	</div>
	<!--mainright-->
	<!-- END OF RIGHT PANEL -->

	<div class="clearfix"></div>

	<div class="footer">
		<div class="footerleft">Computeamove Administration Backend v1.0</div>
		<div class="footerright">&copy; <?php echo date("Y"); ?> Computeamove - All Rights Reserved</a></div>
	</div>
	<!--footer-->


</div>

<!--mainwrapper-->
<script type="text/javascript">
	jQuery(document).ready(function () {
/*
		// basic chart
		var flash = [
			[0, 5],
			[1, 6],
			[2, 4],
			[3, 8],
			[4, 9],
			[5, 13],
			[6, 13]
		];
		var html5 = [
			[0, 2],
			[1, 4],
			[2, 3],
			[3, 1],
			[4, 5],
			[5, 10],
			[6, 8]
		];

		function showTooltip(x, y, contents) {
			jQuery('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 5
			}).appendTo("body").fadeIn(200);
		}

       
		var plot = jQuery.plot(jQuery("#chartplace2"),
			[
				{ data: flash, label: "Open", color: "#fb6409"},
				{ data: html5, label: "Booked", color: "#096afb"}
			], {
				series: {
					lines: { show: true, fill: true, fillColor: { colors: [
						{ opacity: 0.05 },
						{ opacity: 0.15 }
					] } },
					points: { show: true }
				},
				legend: { position: 'nw'},
				grid: { hoverable: true, clickable: true, borderColor: '#ccc', borderWidth: 1, labelMargin: 10 },
				yaxis: { min: 0, max: 15 }
			});

		var previousPoint = null;
		jQuery("#chartplace2").bind("plothover", function (event, pos, item) {
			jQuery("#x").text(pos.x.toFixed(2));
			jQuery("#y").text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint != item.dataIndex) {
					previousPoint = item.dataIndex;

					jQuery("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(2);

					showTooltip(item.pageX, item.pageY,
						item.series.label + " of " + x + " = " + y);
				}

			} else {
				jQuery("#tooltip").remove();
				previousPoint = null;
			}

		});

		jQuery("#chartplace2").bind("plotclick", function (event, pos, item) {
			if (item) {
				jQuery("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
				plot.highlight(item.series, item.datapoint);
			}
		});


		// bar graph
		var d2 = [];
		for (var i = 0; i <= 10; i += 1)
			d2.push([i, parseInt(Math.random() * 30)]);
        console.log(d2);
		
		d2  = [
		 [0,600],
		 [1,1400],
		 [2,900],
		 [3,1000],
		 [4,1600],
		 [5,1300],
		 [6,2000]
       ]
		console.log(parseInt(Math.random() * 30));
		var stack = 0, bars = true, lines = false, steps = false;
		jQuery.plot(jQuery("#bargraph2"), [ d2 ], {
			series: {
				stack: stack,
				lines: { show: lines, fill: true, steps: steps },
				bars: { show: bars, barWidth: 0.6 }
			},
			grid: { hoverable: true, clickable: true, borderColor: '#bbb', borderWidth: 1, labelMargin: 5 },
			colors: ["#06c"]
		});
*/
		// calendar
		//jQuery('#calendar').datepicker();

	var eventDates = {};
	var dates = [];
	jQuery.ajax({
	   url:'home/quotes',
	   type:'GET',
	   success:function(data){
	     //console.log(data);
		 var data =  JSON.parse(data);
		 console.log(data);
		 jQuery.each(data.success,function(key,value){
		    if(moment(value.AgreementDate).isValid()){
			      dates.push({'date':value.RequestedLoadDate});
				 }
		 })
		 var eventDates = jQuery.map(dates, function (val) {
			return jQuery.datepicker.parseDate('yy-mm-dd', val.date.split(' ').shift()).getTime();
		});

		jQuery('#calendar').datepicker({
			beforeShowDay: function (date) {
				return eventDates.indexOf(date.getTime()) != -1 ? [true, "event", date] : [true, '', '']
			},
			onSelect: function (dateText, inst) {
				location.href = '/schedule';
			}
		});
	   }

	})
   });
</script>

</body>
</html>


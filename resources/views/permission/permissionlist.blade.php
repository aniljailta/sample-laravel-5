<div class="rowhere">
					 <div class="divider15"></div>
					  <div class="col-sm-6">
				      <h4 class="widgettitle grid" style="width:87%;">Permission List</h4>
					 <table id="example" class="table table-bordered permission_list_grid">
						  <thead>
							<tr>
								<th>Permission Level Name</th>
							</tr>
						  </thead>
						  <tbody>
							@foreach($permissionLevels as $levels)
							  <tr id="{{$levels->permission_code}}">
							     <td>
								   <span class="hexa" data-code="{{$levels->permission_code}}" id="{{$levels->getAttribute('permission-level_id')}}">{{$levels->permission_label}}
								   </span>
								 </td>
							 </tr>
							@endforeach
						  </tbody>
					 </table>
					 <p style="margin-left: 10px;float: left;width: 90%;text-align: left;margin-top: 20px;">
					        <button type="button" class="btn btn-default" style="margin-right: 10px;" id="newpermission">New</button>
							<button type="button" class="btn btn-default" style="margin-right: 10px;" id="editpermission">Edit</button>
							<button type="button" id="removepermission" class="btn btn-default " style="margin-right: 10px;">Delete</button>
					     </p>
                     </div>
					</div>
      <div class="rowhere">
	             <div class="text-right">
				     <input type="checkbox" id="selectAll">Select All
				 </div>
				 <div class="divider15"></div>
	            <form role="form" id="add_Edit_components" method="post">
  		           <input type="hidden" name="_token" value="{{csrf_token()}}">
				   <table class="table table-bordered display" id="component-grid">
									<colgroup>
										<col class="con0" />
									</colgroup>
									<thead>
									<tr>
										<th></th>
									</tr>
								</thead>
									<tbody>
									 @foreach( $viewElements as $elements)
										  <? if($elementGroup == null || $elementGroup != $elements->element_group) {?>
										  <tr class="cus-col-sm-12">
						                  <td>
						                  	<h3 style="float:left; width:100%">{{$elements->element_group}}</h3>
						                  </td>
						                  </tr>
						                  <? }?>
										<tr class="cus-col-sm-6">
											<td class="leftalign ">
											 <span class="formwrapper">
											    <input type="checkbox" name="elements[]" class="elements" value="{{$elements->getAttribute('view-element_id')}}"  id="element_{{$elements->getAttribute('view-element_id')}}">
											 </span>{{$elements->description}} <!--    <i class="icon-arrow-right"></i> {{$elements->element_group}} -->
											 <? $elementGroup = $elements->element_group?>
											</td>
										</tr>
									@endforeach
                    </tbody>
                </table>
					<div class="text-right"><!--You can add col-lg-12 if you want -->
						<button class="btn-default btn">Save</button>
					</div>
				</form>
		</div>
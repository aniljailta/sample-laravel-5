                        <div class="rowhere">
							<div class="text-right">
							  <input type="checkbox" id="selectAllResource"> Select All
							</div>
							<div class="divider15"></div>

                            <form role="form" id="addreources_role" method="post">
							 <input type="hidden" name="_token" value="{{csrf_token()}}">
							 <table class="table table-bordered" id="resources-grid">
									<colgroup>
										<col class="con0" />
									</colgroup>
									<thead>
									<tr>
										<th></th>
									</tr>
								</thead>
									<tbody>
									@foreach( $routes as $route)
										@if(! in_array($route->getUri(),$openRoutes))
										<tr class="cus-col-sm-3">
											<td class="leftalign"><span class="formwrapper"><input type="checkbox"name="routes[]" class="resources" value="{{$route->getMethods()[0]}}->{{$route->getUri()}}" id="{{$route->getMethods()[0]}}-{{str_replace('/','-',$route->getUri())}}" /></span>
											@if(isset($route->getAction()['as']))
															  {{$route->getAction()['as']}}
															@else
															  {{$route->getMethods()[0]}} - {{$route->getUri()}}
															@endif
											</td>
										</tr>
									@endif
								@endforeach
                    </tbody>
                </table>
				               <div class="divider15"></div>
									<div class="text-right"><!--You can add col-lg-12 if you want -->
										<button class="btn-default btn">Save</button>
									</div>
								<div class="divider15"></div>

							</form>
						</div>
					<div class="rowhere">
					 <div class="divider15"></div>
					  <div class="col-sm-6">
				       <h4 class="widgettitle grid" style="width:87%;">Carriers Cloned Data Status </h4>

					   <table id="default_data_Set" class="table table-bordered display" >
						  <thead>
							<tr>
								<th>Carrier Name</th>
								<th>Status</th>
							</tr>
						  </thead>
						  <tbody>
							@foreach ($carriers as $carrier)
							  <tr>
							     <td  style="width:65%;">
								   <span id="hexa" class="{{$carrier->carrier_id }}">{{$carrier->company_web_name }} </span>
								  
								 </td>
								 <td  style="width:35%">
								  {{ count($carrier->defaultdata) ? 'Cloned' : 'Not Cloned'}}
								    
								 </td>
							 </tr>
							@endforeach
						  </tbody>
					 </table>
					 <p style="margin-left: 10px;float: left;width: 90%;text-align: left;margin-top: 20px;">
					        <button type="button" class="btn btn-default" style="margin-right: 10px;" id="clonedata" >Clone</button>
							</p>
					 </div>
					</div>
					<script type="text/javascript" src="/js/cloneddata.js"></script>
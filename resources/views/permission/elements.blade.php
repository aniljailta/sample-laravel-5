					<div class="rowhere">
					 <div class="divider15"></div>
					  <div class="col-sm-6">
				       <h4 class="widgettitle grid" style="width:87%;">All Components</h4>

					   <table id="permission_elements" class="table table-bordered display" >
						  <thead>
							<tr>
								<th>Element Group</th>
								<th>Element Description</th>
							</tr>
						  </thead>
						  <tbody>
							@foreach ($viewElementsAll as $element)
							  <tr>
							     <td  style="width:35%;">
								   <span class="hexa" id="{{$element->getAttribute('view-element_id')}}">{{ $element->element_group }}
								   </span>
								 </td>
								 <td  style="width:65%">
								  {{ $element->description }}
								 </td>
							 </tr>
							@endforeach
						  </tbody>
					 </table>
					 <p style="margin-left: 10px;float: left;width: 90%;text-align: left;margin-top: 20px;">
					        <button type="button" class="btn btn-default" style="margin-right: 10px;" id="newelement">New</button>
							<button type="button" class="btn btn-default" style="margin-right: 10px;" id="editelement">Edit</button>
							<button type="button" id="deleteelement" class="btn btn-default " style="margin-right: 10px;">Delete</button>
					     </p>
                     </div>
					</div>
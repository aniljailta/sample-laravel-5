					<div class="rowhere">
					 <div class="divider15"></div>
					  <div class="col-sm-6">
				       <h4 class="widgettitle grid" style="width:87%;">Open Routes</h4>
					   <table id="permission_open_routes" class="table table-bordered">
						  <thead>
							<tr>
								<th>URI</th>
								<th>Method</th>
							</tr>
						  </thead>
						  <tbody>
							@foreach($openRoutesAll as $route)
							  <tr>
							     <td>
								   <span class="hexa" id="{{$route->getAttribute('open-route_id')}}">{{ $route->route }}
								   </span>
								 </td>
								 <td style="width:20%;">
								  {{ $route->method }}
								 </td>
							 </tr>
							@endforeach
						  </tbody>
					 </table>
					 <p style="margin-left: 10px;float: left;width: 90%;text-align: left;margin-top: 20px;">
					        <button type="button" class="btn btn-default" style="margin-right: 10px;" id="newopenRoute">New</button>
							<!--<button type="button" class="btn btn-default" style="margin-right: 10px;" id="editopenRoute">Edit</button>-->
							<button type="button" id="deleteopenRoute" class="btn btn-default " style="margin-right: 10px;">Delete</button>
					     </p>
                     </div>
					</div>
<div class="modal fade" id="PermissionAddModal" tabindex="-1" role="dialog" aria-labelledby="PermissionAddModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="PermissionAddLabel"></h4>
			</div>
			<div class="modal-body">
			<form role="form" id="add_permission_modal" class="form-horizontal">
				  <input type="hidden" name="_token" id="token_id" value="{{ csrf_token() }}">
				  <input type="hidden" name="PermissionId" >
                  <p style="float: left; margin-left: 10px;">
					<label style="text-align: left;">Role type</label>
					<span class="field" style="margin-left: 0px;">
						<select name="PermissionType" id="is_type" class="uniformselect">
							<option value="general">General</option>
							<option value="company">Company Affiliated</option>
						</select>
					</span>
				  </p>
				  <p style="float: left; margin-left: 10px;">
					<label>Permission Label</label>
					<span class="field">
					  <input type="text" name="PermissionName" class="input-medium" placeholder="Permission Name" required/>
					</span>
				  </p>
				  <div id="rowhereall">
					   <button type="button" id="reset_permission" class="btn btn-default" style="margin-left: 10px;float:left;">Reset</button>
					   <button type="submit" id="save_new_permission" class="btn btn-default" >Save</button>
				 </div>
				</div>
		    </form>
		</div>
	</div>
  </div>
  <div class="modal fade" id="OpenRouteModal" tabindex="-1" role="dialog" aria-labelledby="OpenRouteModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="OpenRouteLabel"></h4>
			</div>
			<div class="modal-body">
			<form role="form" id="add_open_route" method="post">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				  <p style="float: left; margin-left: 10px;">
						<label>Resource</label>
						<select class="form-control" name="route">
						  @foreach( $routes as $route)
							@if(! in_array($route->getUri(),$openRoutes))
							  <div class="checkbox" style="float: left; width: 281px; margin-right: 40px;">
									@if(isset($route->getAction()['as']))
									  <option value="{{$route->getUri()}}">{{$route->getAction()['as']}}</option>
									@else
									  <option value="{{$route->getUri()}}">{{$route->getMethods()[0]}} - {{$route->getUri()}}</option>
									@endif
							  </div>
							@endif
						@endforeach
						</select>
					</p>

					<p style="float: left; margin-left: 30px;">
						<label>Method</label>
						<select class="form-control" name="method">
						  <option value="GET">GET</option>
						  <option value="POST">POST</option>
						  <option value="PUT">PUT</option>
						  <option value="DELETE">DELETE</option>
						  <option value="PATCH">PATCH</option>
						</select>
					</p>
				  <div id="rowhereall">
					   <!--<button type="button" id="reset_permission" class="btn btn-default" style="margin-left: 10px;float:left;">Reset</button>-->
					   <button type="submit" id="save_new_permission" class="btn btn-default" >Save</button>
				 </div>
				</div>
		    </form>
		</div>
	</div>
  </div>
  <!----Modal for adding and updating elements --->
  
  <div class="modal fade" id="AddElementModal" tabindex="-1" role="dialog" aria-labelledby="AddElementModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="AddElementLabel"></h4>
			</div>
			<div class="modal-body">			
			<form role="form" id="add_elements" method="post">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					 <p style="float: left; margin-left: 30px;">
						 <label for="elementGroup">Element Group</label>
						<select name="elementGroup" id="">
							@foreach($groups as $group)
							<option value="{{$group}}">{{$group}}</option>
							@endforeach
						</select>
						<input type="text" name="newElementGroup" class="form-control" id="" placeholder="Enter new group name">
					  </p>
					 <p style="float: left; margin-left: 30px;">
						
					  <label for="elementName">Element Name</label>
						<input type="text" name="elementName" class="form-control" id="" placeholder="Enter element name" required>
					  </p>
					  <p style="float: left; margin-left: 30px;">						
					    <label for="description">Description</label>
						<textarea name="description" id="" cols="50" rows="10" required></textarea>
					  </p>
					  
					  <div id="rowhereall">					   
						  <button type="submit" id="save_new_permission" class="btn btn-default" >Save</button>
					 </div>					
				</div>
		    </form>
		</div>
	</div>
  </div>
  
  
  
  <!----Modal for adding and updating elements --->
  
  <div class="modal fade" id="EditElementModal" tabindex="-1" role="dialog" aria-labelledby="EditElementModal" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="EditElementLabel"></h4>
			</div>
			<div class="modal-body">			
			<form role="form" id="edit_elements" method="post">
					 <input type="hidden" name="_token" value="{{csrf_token()}}">
					  <p style="float: left; margin-left: 30px;">						
					    <label for="description">Description</label>
						<textarea name="description" id="elementDescription" cols="50" rows="10" required></textarea>
					  </p>
					  
					  <div id="rowhereall">	
                          <button type="button" id="reset_edit_element" class="btn btn-default" style="margin-left: 10px;float:left;">Reset</button>					  
						  <button type="submit" class="btn btn-default" >Save</button>
					 </div>					
				</div>
		    </form>
		</div>
	</div>
  </div>